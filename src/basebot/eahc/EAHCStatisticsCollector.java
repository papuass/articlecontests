/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.eahc;

import com.google.gson.*;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Map.*;
import java.util.logging.*;
import basebot.ceespring.objects.CEESTableLine;
import basebot.ceespring.objects.CEESUser;
import objects.EAHCArticle;
import objects.EAHCElement;
import org.wikipedia.BaseBot;
import org.wikipedia.Wiki.Revision;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class EAHCStatisticsCollector {

    /**
     * @param args the command line arguments
     */
    static Set<String> theLists = new HashSet<>();
    static Set<CEESTableLine> rows = new HashSet<>();
    static Set<String> qs = new HashSet<>();
    static Map<String, Map<String, CEESUser>> wikisusers = new HashMap<>();
    static Map<String, BaseBot> wikis = new HashMap<>();
    static Set<EAHCArticle> articles = new HashSet<>();
    static Map<String, String> users = new HashMap<>();
    static Set<EAHCElement> elements = new HashSet<>();

    public static void main(String[] args) throws Exception {
        BaseBot d = new BaseBot("www.wikidata.org");
        d.setUserAgent("BaseBot");
        d.login(args[0], args[1]);
        d.setMarkBot(true);
        d.setMarkMinor(true);
        String config = d.getPageText("User:Wittylama/280 config.js").replaceAll("(?s)[^\\{]*(\\{.*\\})[^\\}]*", "$1");
        System.out.println(config);
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(config);
        JsonObject alljson = element.getAsJsonObject();
        JsonObject configuration = alljson.getAsJsonObject("configuration");
        //  languageslisting
        JsonObject languagesJson = configuration.getAsJsonObject("languages");
        Set<String> languagesSet = new HashSet<>();
        Set<Map.Entry<String, JsonElement>> entrySet = languagesJson.entrySet();
        Entry<String, JsonElement>[] entries = entrySet.toArray(new Entry[entrySet.size()]);
        for (int i = 0; i < entries.length; i++) {
            Entry<String, JsonElement> entry = entries[i];
            String key = entry.getKey();
            languagesSet.add(key);
            System.out.println(i + "\t" + key);
        }
        String[] languages = languagesSet.toArray(new String[languagesSet.size()]);
        // countries reversed listing
        JsonObject countriesJson = configuration.getAsJsonObject("countries");
        Set<Map.Entry<String, JsonElement>> countryEntrySet = countriesJson.entrySet();
        Entry<String, JsonElement>[] countryEntries = countryEntrySet.toArray(new Entry[countryEntrySet.size()]);
        Map<String, String> countriedArticles = new HashMap<>();
        for (int i = 0; i < countryEntries.length; i++) {
            Entry<String, JsonElement> countryEntry = countryEntries[i];
            String countryCode = countryEntry.getKey();
            JsonObject countryJson = countriesJson.getAsJsonObject(countryCode);
            System.out.println(countryCode);
            String country = countryJson.getAsJsonPrimitive("label").getAsString();
            System.out.println(i + "\t" + country);
            JsonArray items = countryJson.getAsJsonArray("items");
            for (int j = 0; j < items.size(); j++) {
                String wd = items.get(j).getAsString();
                System.out.println(i + "\t" + country + "\t" + wd);
                if (!countriedArticles.containsKey(wd)) {
                    countriedArticles.put(wd, country);
                } else {
                    countriedArticles.put(wd, countriedArticles.get(wd) + ", " + country);
                }
            }
        }
        Set<String> countriedArticlesKeys = countriedArticles.keySet();
        String[] wds = countriedArticlesKeys.toArray(new String[countriedArticlesKeys.size()]);
        for (String wdEntity : wds) {
            //Wikidata entity considered
            //Analysing Wikidata entity's labels and descriptions additions
            Revision[] entityHistory = d.getPageHistory(wdEntity); //All the time is included, not just the contest span boundaried
            for (Revision entityRevision : entityHistory) {
                String entityRevisionSummary = entityRevision.getSummary();
                String elementAddRegex = "\\/\\* wbset(label|description)-add\\:1\\|([^ ]*) \\*\\/.*";
                Boolean matchesElementAddRegex = entityRevisionSummary.matches(elementAddRegex);
                if (matchesElementAddRegex) {
                    String elementLanguage = entityRevisionSummary.replaceAll(elementAddRegex, "$2").trim();
                    if (matchesAnyLanguage(elementLanguage, languages)) {
                        String elementCountry = countriedArticles.get(wdEntity);
                        String elementType = entityRevisionSummary.replaceAll(elementAddRegex, "$1").trim();
                        String elementUser = entityRevision.getUser();

                        String elementUserid = "";
                        String elementCAId = "";
                        proceedUser(elementUser, d, "wikidatawiki"); // puts user's local and CentralAuth ids into global variable
                        elementUserid = users.get(elementUser + "@" + "wikidatawiki");
                        elementCAId = users.get(elementUser + "@global");
                        Calendar revisionTimestamp = entityRevision.getTimestamp();

                        elements.add(
                                new EAHCElement(
                                        wdEntity, elementUser,
                                        elementUserid, elementCAId,
                                        revisionTimestamp, elementLanguage,
                                        elementCountry, elementType
                                )
                        );
                    } else {
                        //language is of no interest
                    }
                } else {
                    //the revision is of no interest
                }
            }
            // Langlinks reading and further articles analysing
            Map< String, String> langlinks = d.getSiteLinks(wdEntity);
            Set<String> keySet = langlinks.keySet();
            System.out.println(langlinks.size());
            Set<String> wikidbs = new HashSet<>();
            wikidbs.addAll(keySet);
            String[] keys = wikidbs.toArray(new String[wikidbs.size()]);
            Set<Thread> fabric = new HashSet<>();
            wdwikisiterator:
            for (String key : keys) {
                if (!key.matches("[_a-z]+wiki")) {
                    continue wdwikisiterator;
                }
                String wname = key.substring(0, key.indexOf("wiki")).replaceAll("_", "-") + ".wikipedia.org";
                boolean matchesanylanguage = false;
                iteratelanguages:
                for (String language : languages) {
                    if ((language + ".wikipedia.org").equals(wname)) {
                        matchesanylanguage = true;
                        break iteratelanguages;
                    }
                }
                if (!matchesanylanguage) {
                    continue wdwikisiterator;
                }
                BaseBot w;
                if (wikis.containsKey(wname)) {
                    w = wikis.get(wname);
                } else {
                    w = new BaseBot(wname);
                    w.login(args[0], args[1]);
                    w.setMarkBot(true);
                    w.setMarkMinor(true);
                    wikis.put(wname, w);
                }
                String article = langlinks.get(key);

                final String wname_f = wname;
                final String article_f = article;
                final String wd_f = wdEntity;
                final BaseBot w_f = w;
                final Map<String, String> countriedArticles_f = countriedArticles;
                Runnable runnable;
                runnable = () -> {
                    try {
                        workInWiki(wname_f, article_f, wd_f, w_f, countriedArticles_f);
                    } catch (Exception ex) {
                        Logger.getLogger(EAHCStatisticsCollector.class.getName()).log(Level.SEVERE, null, ex);
                        System.exit(666);
                    }
                };
                Thread thread = new Thread(runnable);
                thread.setName(wname);
                fabric.add(thread);
                thread.start();
            }
            for (Thread t : fabric) {
                System.out.println(t.getName());
                t.join();
            }
        }
        writeArticlesToDB();
        writeElementsToDB();
    }

    public static void workInWiki(String wname, String article, String wd, BaseBot w, Map<String, String> countriedArticles) throws Exception {
        System.out.println(wname + "\t" + Thread.currentThread().getName());

        String page = article;
        Revision[] pageHistory = w.getPageHistory(page);
        Revision firstRevision = pageHistory[pageHistory.length - 1];
        Calendar timestamp = firstRevision.getTimestamp();
        Calendar start;
        start = new GregorianCalendar(2016, 3, 14, 00, 00, 00);
        int startoff = timestamp.compareTo(start);
        if (startoff < 0) {
            return;
        }

        Calendar end;
        end = new GregorianCalendar(2016, 5, 1, 23, 59, 59);
        int endoff = timestamp.compareTo(end);
        if (startoff > 0) {
            return;
        }
        String pageid = ((long) w.getPageInfo(page).get("pageid")) + "";
        String user = firstRevision.getUser();

        String userid = "";
        String CAId = "";
        proceedUser(user, w, wname); // puts user's local and CentralAuth ids into global variable
        userid = users.get(user + "@" + wname);
        CAId = users.get(user + "@global");

        System.out.println("User proceeded:\t" + user + "\t" + CAId + "\t" + userid
        );
        String language = wname.substring(0, wname.indexOf('.'));
        String country = countriedArticles.get(wd);
        String wikidata = wd;
        EAHCArticle eahcArticle = new EAHCArticle(page, pageid, user, userid, CAId, timestamp, language, country, wikidata);
        addToArticles(eahcArticle);
    }//end of work in wiki

    /**
     * Writes all the stuff we fetched to our localhost database so that we can
     * proceed data later for the outputs. Reads all the stuff off global
     * variables, thus no input params are used.
     *
     * @throws Exception
     */
    public static void writeArticlesToDB() throws Exception {
        Connection conn;
        String myDriver = "org.gjt.mm.mysql.Driver";
        String myUrl = "jdbc:mysql://localhost/" + "eahc" + "?characterEncoding=utf8";
        Class.forName(myDriver);
        conn = DriverManager.getConnection(myUrl, "root", "");
        String query = "TRUNCATE `" + 2016 + "`";
        Statement st = conn.createStatement();
        st.execute(query);
        System.out.println("We are going to database\t" + articles.size() + "\tarticles");
        int index = 0;
        for (EAHCArticle ar : articles) {
            System.out.println((++index) + "\tWe are databasing\t" + ar.articlename);
            query = " insert into `2016` ("
                    + " article_name,"
                    + " article_id,"
                    + " user_name,"
                    + " user_id,"
                    + " user_ca,"
                    + " article_creationtime,"
                    + " article_language,"
                    + " article_country,"
                    + " article_wikidata"
                    + ")"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, ar.articlename);
            preparedStmt.setString(2, ar.curid);
            preparedStmt.setString(3, ar.user);
            preparedStmt.setString(4, ar.userid);
            preparedStmt.setString(5, ar.userca);
            preparedStmt.setTimestamp(6, new java.sql.Timestamp(ar.timestamp.getTime().getTime()));
            preparedStmt.setString(7, ar.language);
            preparedStmt.setString(8, ar.country);
            preparedStmt.setString(9, ar.wikidata);
            preparedStmt.execute();
        }
        conn.close();
    }

    public static void writeElementsToDB() throws Exception {
        Connection conn;
        String myDriver = "org.gjt.mm.mysql.Driver";
        String myUrl = "jdbc:mysql://localhost/" + "eahc" + "?characterEncoding=utf8";
        Class.forName(myDriver);
        conn = DriverManager.getConnection(myUrl, "root", "");
        String query = "TRUNCATE `" + 2016 + "_elements`";
        Statement st = conn.createStatement();
        st.execute(query);
        System.out.println("We are going to database\t" + elements.size() + "\telements");
        int index = 0;
        for (EAHCElement el : elements) {
            System.out.println((++index) + "\tWe are databasing\t" + el.element);
            query = " insert into `2016_elements` ("
                    + " element,"
                    + " user_name,"
                    + " user_id,"
                    + " user_ca,"
                    + " element_creationtime,"
                    + " element_language,"
                    + " element_country,"
                    + " element_type"
                    + ")"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, el.element);
            preparedStmt.setString(2, el.user);
            preparedStmt.setString(3, el.userid);
            preparedStmt.setString(4, el.userca);
            preparedStmt.setTimestamp(5, new java.sql.Timestamp(el.timestamp.getTime().getTime()));
            preparedStmt.setString(6, el.language);
            preparedStmt.setString(7, el.country);
            preparedStmt.setString(8, el.type);
            preparedStmt.execute();
        }
        conn.close();
    }

    /**
     *
     * @param article
     */
    public synchronized static void addToArticles(EAHCArticle article) {
        articles.add(article);
    }

    /**
     *
     * @param user
     * @param id
     */
    public synchronized static void addToUsers(String user, String id) {
        users.put(user, id);
    }

    /**
     * Resolves redirects for Wikibase repository (Wikidata) entities
     *
     * @param d - BaseBot instance for Wikibase repository (Wikidata)
     * @throws IOException
     */
    public static void entityRedSolution(BaseBot d) throws IOException {
        String[] qses = qs.toArray(new String[qs.size()]);
        qs.clear();
        String query = "";
        for (String string : qses) {
            query += "".equals(query) ? string : "|" + string;
        }
        String[] qids = d.WDEntity(query.split("\\|"));
        theLists.addAll(Arrays.asList(qids)); //System.out.println(j + "\t" + string);
    }

    /**
     *
     * @param user
     * @param w
     * @param userwiki
     * @throws IOException
     */
    public static synchronized void proceedUser(String user, BaseBot w, String userwiki) throws IOException {
        String userid;
        String CAId;
        // for ipv6 http://stackoverflow.com/questions/53497/regular-expression-that-matches-valid-ipv6-addresses was used.
        if (!(user.matches("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z")
                || user.matches("(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}"
                        + "|([0-9a-fA-F]{1,4}:){1,7}:"
                        + "|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}"
                        + "|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}"
                        + "|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}"
                        + "|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}"
                        + "|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}"
                        + "|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})"
                        + "|:((:[0-9a-fA-F]{1,4}){1,7}|:)"
                        + "|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}"
                        + "|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]"
                        + "|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]"
                        + "|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])"
                        + "|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]"
                        + "|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]"
                        + "|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"))) {
            if (users.containsKey(user + "@" + userwiki)) {
            } else {
                userid = (String) w.getUser(user).getUserInfo().get("userid");
                users.put(user + "@" + userwiki, userid);
            }
            if (users.containsKey(user + "@global")) {
            } else {
                CAId = w.CAId(user) + "";
                users.put(user + "@global", CAId);
            }
        } else {// an anon
            userid = "N/A";
            CAId = "N/A";
            if (users.containsKey(user + "@" + userwiki)) {
            } else {
                users.put(user + "@" + userwiki, userid);
            }
            if (users.containsKey(user + "@global")) {
            } else {
                users.put(user + "@global", CAId);
            }
        }
    }

    public static boolean matchesAnyLanguage(String givenLanguage, String[] languages) {
        boolean result = false;
        for (String iteratedLanguage : languages) {
            if ((iteratedLanguage).equals(givenLanguage)) {
                result = true;
                break;
            }
        }
        return result;
    }
}//Class end
