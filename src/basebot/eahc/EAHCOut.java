/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.eahc;

import basebot.ceespring.*;
import com.google.gson.Gson;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import objects.EAHCParam;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class EAHCOut {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot d = new BaseBot("www.wikidata.org");
        d.login(args[0], args[1]);
        d.setMarkBot(true);
        d.setMarkMinor(true);

        DatabasePart db = new DatabasePart("eahc");

        String wrt = "";

        String query = ""
                + "SELECT *\n"
                + "FROM `2016`\n"
                + "ORDER BY `2016`.`article_creationtime` DESC\n";

        System.out.println(query);

        ResultSet generalOutputtie = db.generalOutputtie(query);

        List<String> tablerows = new ArrayList<>();

        String table = "{| class=\"wikitable sortable\"\n"
                + "! Counter\n"
                + "! Article\n"
                + "! User\n"
                + "! Creation time\n"
                + "! Language\n"
                + "! Country\n"
                + "! Wikidata\n";

        while (generalOutputtie.next()) {
            //generalOutputtie.getString(1);
            String tablerow = "";
            tablerow += "| [[:" + generalOutputtie.getString(8) + ":" + generalOutputtie.getString(2) + "|" + generalOutputtie.getString(2) + "]]"
                    + "<br /><small>"
                    + generalOutputtie.getString(3)
                    + "</small>\n";
            tablerow += "| [[:" + generalOutputtie.getString(8) + ":User:" + generalOutputtie.getString(4) + "|" + generalOutputtie.getString(4) + "]]"
                    + "<br /><small>"
                    + generalOutputtie.getString(5) + " &bull; " + generalOutputtie.getString(6)
                    + "</small>\n";
            tablerow += "| " + generalOutputtie.getString(7) + "\n";
            tablerow += "| {{#language:" + generalOutputtie.getString(8) + "|{{CURRENTCONTENTLANGUAGE}}}}\n";
            tablerow += "| " + generalOutputtie.getString(9) + "\n";
            tablerow += "| [[d:" + generalOutputtie.getString(10) + "|" + generalOutputtie.getString(10) + "]]\n";
            tablerows.add(tablerow);
        }

        int counter = tablerows.size();
        for (String tablerow : tablerows) { // We want to assign chronological counter
            tablerow = ""
                    + "|-\n"
                    + "| " + (counter--) + "\n"
                    + tablerow;
            table += tablerow;
        }
        table += "|}";

        String[] params = {"language", "country"};
        for (String param : params) {
            String query2 = ""
                    + "SELECT `article_" + param + "`, COUNT(*) as articles\n"
                    + "FROM `2016`\n"
                    + "GROUP BY `article_" + param + "`\n"
                    + "ORDER BY articles desc";
            System.out.println(query2);

            String graph = "{{User:BaseBot/EAHC/MMXVI/General, per country and per language statistics/Params template|param=" + param + "}}";
            String pie = "{{Graph:Chart"
                    + "|width=100"
                    + "|height=100"
                    + "|type=pie"
                    + "|legend=Legende";

            String x = "|x=";
            String y = "|y=";
            String y1 = "|y1=";
            boolean first = true;
            String paramtable = "{| class=\"wikitable sortable\"\n"
                    + "! " + param + "\n"
                    + "! articles\n";
            ResultSet anotherOutputtie = db.generalOutputtie(query2);

            List<EAHCParam> paramsList = new ArrayList<>();

            while (anotherOutputtie.next()) {
                x += (first ? "" : ",") + anotherOutputtie.getString(1);
                y1 += (first ? "" : ",") + anotherOutputtie.getString(2);
                first = false;
                paramtable += "|-\n"
                        + "| " + (param.equals("language") ? "{{#language:" : "")
                        + anotherOutputtie.getString(1)
                        + (param.equals("language") ? "|{{CURRENTCONTENTLANGUAGE}}}}" : "")
                        + "\n"
                        + "| " + anotherOutputtie.getString(2) + "\n";

                paramsList.add(new EAHCParam(anotherOutputtie.getString(1), anotherOutputtie.getInt(2)));
            }
            paramtable += "|-\n"
                    + "! &Sigma;\n"
                    + "! " + tablerows.size() + "\n"
                    + "|}";
            pie += x + y1 + "|showValues=}}";

            wrt += "=== By " + param + " ===\n{|\n|-\n|\n";
            wrt += paramtable + "\n|\n";
            wrt += graph + "\n";
            wrt += pie + "\n|}\n";

            Gson gson = new Gson();
            String toJson = gson.toJson(paramsList);
            System.out.println(toJson);
            d.edit("User:BaseBot/EAHC/MMXVI/General, per country and per language statistics/Params template/" + param, toJson, "creating/updating");

            wrt += timeChart("article_" + param, "article_creationtime", "Articles written within contest timespan to date", "", "2016", db);
            wrt += timeChart("element_" + param, "element_creationtime", "Labels manual additions to date", "WHERE `element_type` = \"label\"\n", "2016_elements", db);
            wrt += timeChart("element_" + param, "element_creationtime", "Descriptions manual additions to date", "WHERE `element_type` = \"description\"\n", "2016_elements", db);

        }//end of per params

        //labels and descriptions graphs
        wrt += "=== All articles ===\n"
                + "Lists all the articles created since 14th of April, ordered by creation date with the newest at the top.\n";
        wrt += table;

        System.out.println(wrt);

        d.edit("user:BaseBot/EAHC/MMXVI/General, per country and per language statistics", wrt, "creating/updating");

        db.stopDB();
    }

    public static String timeChart(String selectie, String creationTimeColumn, String selectiesName, String where, String table, DatabasePart db) throws Exception {

        String paramsQuery = ""
                + "SELECT `" + selectie + "`,\n"
                + "COUNT(*) as selecties,\n";

        Calendar date = new GregorianCalendar(2016, 03, 14, 00, 00, 00);
        Calendar tomorrow = new GregorianCalendar();
        tomorrow.add(Calendar.DATE, 1);
        List<String> days = new ArrayList<>();
        boolean condition = date.compareTo(new GregorianCalendar(2016, 05, 1, 23, 59, 59)) <= 0
                && date.compareTo(tomorrow) <= 0;
        while (condition) {
            date.add(Calendar.DATE, 1);
            condition = date.compareTo(new GregorianCalendar(2016, 05, 1, 23, 59, 59)) <= 0
                    && date.compareTo(tomorrow) <= 0;
            String year = date.get(Calendar.YEAR) + "";
            String month = ((date.get(Calendar.MONTH) + 1) < 10 ? "0" : "") + (date.get(Calendar.MONTH) + 1);
            String day = (date.get(Calendar.DATE) < 10 ? "0" : "") + date.get(Calendar.DATE);
            String hour = (date.get(Calendar.HOUR) < 10 ? "0" : "") + date.get(Calendar.HOUR);
            String minute = (date.get(Calendar.MINUTE) < 10 ? "0" : "") + date.get(Calendar.MINUTE);
            String second = (date.get(Calendar.SECOND) < 10 ? "0" : "") + date.get(Calendar.SECOND);
            paramsQuery += "COUNT(CASE WHEN `" + creationTimeColumn + "` < "
                    + year
                    + month
                    + day
                    + hour
                    + minute
                    + second
                    + " THEN 1 ELSE NULL END) as \"" + day + month + "\"" + (condition ? "," : "") + "\n";
            days.add(day + "." + month);
        }
        paramsQuery += "FROM `" + table + "`\n"
                + where
                + "GROUP BY `" + selectie + "`\n"
                + "ORDER BY selecties desc";

        System.out.println(paramsQuery);

        String timechart = "{{Graph:Chart"
                + "|width=1000"
                + "|height=500"
                + "|xAxisTitle=Days"
                + "|xType=string"
                + "|yAxisTitle=" + selectiesName
                + "|legend=Legend"
                + "|type=line";
        String tx = "|x=";
        String ty = "";
        String titles = "";
        boolean first = true;
        for (String day : days) {
            tx += (first ? "" : ",") + day;
            first = false;
        }
        ResultSet timelineOutput = db.generalOutputtie(paramsQuery);

        int paramcounter = 0;
        while (timelineOutput.next()) {//I iterate params.
            boolean firstday = true;

            int daycounter = 0;
            paramcounter++;
            titles += "|y" + paramcounter + "Title=" + timelineOutput.getString(1);

            for (String day : days) {
                ty += (firstday ? "|y" + paramcounter + "=" : ",") + timelineOutput.getString(2 + (++daycounter));
                firstday = false;
            }
        }
        timechart += titles + tx + ty
                + "}}";

        System.out.println(timechart);
        String returnString = timechart + "\n";
        return returnString;
    }
}
