/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.out;

import basebot.ceespring.DatabasePart;
import java.sql.ResultSet;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class outPart_bgwiki {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot m = new BaseBot("bg.wikipedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);

        DatabasePart db = new DatabasePart();

        String query = "SELECT `author_name`,\n"
                + "COUNT(DISTINCT `article_id`) AS articles,\n"
                + "SUM(CASE WHEN  `article_isimproved` = \"no\" THEN 1 ELSE 0 END) AS created,\n"
                + "SUM(CASE WHEN  `article_isimproved` = \"yes\" THEN 1 ELSE 0 END) AS improved,\n"
                + "SUM(`article_authorbytessigma`) as bytes,\n"
                + "SUM(CASE WHEN  `article_isperson` = \"yes\" THEN 1 ELSE 0 END) AS people,\n"
                + "SUM(CASE WHEN  `article_iswoman` = \"yes\" THEN 1 ELSE 0 END) AS women,\n"
                + "SUM(CASE WHEN  `article_iswoman` = \"yes\" THEN `article_authorbytessigma` ELSE 0 END) as bytes_about_women\n"
                + "FROM `2016`\n"
                + "WHERE `wiki` = \"bg.wikipedia.org\"\n"
                + "GROUP BY `author_name`\n"
                + "ORDER BY articles DESC";
        System.out.println(query);

        ResultSet generalOutputtie = db.generalOutputtie(query);

        String wrt = "== Резултати ==\n";
        wrt += "Към {{subst:#time:d F в H:i T}}\n";
        wrt += "{{Уикимедия ЦИЕ Пролет 2016/Резултати/Header}}\n";

//| [[Потребител:Drzewianin]]||нова||3||16426||1||0
//: [[:m:User:BaseBot/CEES/MMXVI/Wikis|Автоматично обновяващ се списък на броя статии, статии от списъците, статии за хора, статии за жени, автори, авторки, опитни редактори, нови редактори]]
//: Към {{subst:time|d F в H:i T}}: 120 статии, 46 от списъците, 50 за хора, от тях 37 за жени, 12 участващи автори, от тях 3 жени (според това, което са споменали в настройките си), 11 опитни редактори, 1 нов редактор.
//        
        while (generalOutputtie.next()) {

            wrt += "|-\n";
            wrt += "|[[Потребител:" + generalOutputtie.getString(1) + "]]\n";
            wrt += "|" + generalOutputtie.getInt(2) + "\n";
            wrt += "|" + generalOutputtie.getInt(3) + "\n";
            wrt += "|" + generalOutputtie.getInt(4) + "\n";
            wrt += "|" + generalOutputtie.getInt(5) + "\n";
            wrt += "|" + generalOutputtie.getInt(6) + "\n";
            wrt += "|" + generalOutputtie.getInt(7) + "\n";
            wrt += "|" + generalOutputtie.getInt(8) + "\n";
        }


        String query2
                = "SELECT COUNT(DISTINCT `article_id`) AS articles,\n"
                + "SUM(CASE WHEN  `article_isimproved` = \"no\" THEN 1 ELSE 0 END) AS created,\n"
                + "SUM(CASE WHEN  `article_isimproved` = \"yes\" THEN 1 ELSE 0 END) AS improved,\n"
                + "SUM(CASE WHEN  `article_fromlist` = \"1\" THEN 1 ELSE 0 END) AS fromlist,\n"
                + "SUM(CASE WHEN  `article_isperson` = \"yes\" THEN 1 ELSE 0 END) AS people,\n"
                + "SUM(CASE WHEN  `article_iswoman` = \"yes\" THEN 1 ELSE 0 END) AS woman,\n"
                + "COUNT(DISTINCT `author_name`) AS authors,\n"
                + "(SELECT count(DISTINCT `author_name`) FROM `2016` WHERE `author_gender` = \"female\" AND `wiki` = \"bg.wikipedia.org\") AS females,\n"
                + "(SELECT count(DISTINCT `author_name`) FROM `2016` WHERE `author_isnoob` = \"no\" AND `wiki` = \"bg.wikipedia.org\") AS nonnoobs,\n"
                + "(SELECT count(DISTINCT `author_name`) FROM `2016` WHERE `author_isnoob` = \"yes\" AND `wiki` = \"bg.wikipedia.org\") AS noobs,\n"
                + "SUM(`article_authorbytessigma`) as bytes,\n"
                + "SUM(CASE WHEN  `article_iswoman` = \"yes\" THEN `article_authorbytessigma` ELSE 0 END) as bytes_about_women\n"
                + "FROM `2016`\n"
                + "WHERE `wiki` = \"bg.wikipedia.org\"";

        System.out.println(query2);

        ResultSet totalOuputtie = db.generalOutputtie(query2);

        while (totalOuputtie.next()) {

            wrt += "|-\n";
            wrt += "!Общо\n";
            wrt += "!" + totalOuputtie.getInt(1) + "\n";
            wrt += "!" + totalOuputtie.getInt(2) + "\n";
            wrt += "!" + totalOuputtie.getInt(3) + "\n";
            wrt += "!" + totalOuputtie.getInt(11) + "\n";
            wrt += "!" + totalOuputtie.getInt(5) + "\n";
            wrt += "!" + totalOuputtie.getInt(6) + "\n";
            wrt += "!" + totalOuputtie.getInt(12) + "\n";

            wrt += "|}\n\n";
            wrt += ": [[:m:User:BaseBot/CEES/MMXVI/Wikis|Автоматично "
                    + "обновяващ се списък на броя статии, статии от "
                    + "списъците, статии за хора, статии за жени, автори, "
                    + "авторки, опитни редактори, нови редактори]]\n";
            wrt += ": Към {{subst:#time:d F в H:i T}}: " + totalOuputtie.getInt(1) + " статии, "
                    + totalOuputtie.getInt(4) + " от списъците, " + totalOuputtie.getInt(5) + " за хора, "
                    + "от тях " + totalOuputtie.getInt(6) + " за жени, "
                    + totalOuputtie.getInt(7) + " участващи автори, "
                    + "от тях " + totalOuputtie.getInt(8) + " жени (според това, "
                    + "което са споменали в настройките си), "
                    + totalOuputtie.getInt(9) + " опитни редактори, " + totalOuputtie.getInt(10) + " нов редактор.";
        }

        m.edit("Шаблон:Уикимедия ЦИЕ Пролет 2016/Резултати", wrt, "creating/updating");
        System.out.println(wrt);

        db.stopDB();
    }

}
