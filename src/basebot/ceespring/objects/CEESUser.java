/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.objects;

/**
 * @author Base <base-w at yandex.ru>
 */
public class CEESUser {

    public String name;

    public String gender;
    public String isNoob;
    public String identifier;
    public String caid;


    public CEESUser(String username) {
        this.name = username;
        this.identifier = "N/A";
        this.gender = "N/A";
        this.isNoob = "N/A";
        this.caid = "N/A";
    }


    public CEESUser(String id, String CAId, String username, String sex, String noob) {
        this.name = username;
        this.identifier = id;
        this.gender = sex;
        this.isNoob = noob;
        this.caid = CAId;
    }

//    public CEESUser(String username) {
//        this.name = username;
//    }

    public CEESUser(String id, boolean isid) {
        if (isid) {
            this.identifier = id;
        }
    }

    public void setName(String username) {
        this.name = username;
    }

    public void setID(String id) {
        this.identifier = id;
    }

    public void setGender(String sex) {
        gender = sex;
    }

    public void setNoob(String noob) {
        isNoob = noob;
    }

    public void setCAId(String CAId) {
        caid = CAId;
    }

    public String getName() {
        return name;
    }

    public String getID() {
        return identifier;
    }

    public String isNoob() {
        return isNoob;
    }

    @Override
    public String toString() {
        return "CEE Spring participant " + name + " (ID: " + identifier + "), " + gender + ", is noob: " + isNoob + ")";
    }

}
