/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring;

import java.math.BigDecimal;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class CEESMMXVIUAMarkedArticle {

    //Gathered data
    public String articleID;
    public String articleName;

    public String author;

    public int added;

    public String topic;
    public int week;
    public String badges;

    public Boolean fromLists;
    public Boolean isList;
    public Boolean isDisqualified;

    public BigDecimal Krutyvuss;
    public BigDecimal VolodymyrDk;
    public BigDecimal Visem;
    public BigDecimal JurorYon;

    //Computed data
    public BigDecimal qualityCoefficient;
    public BigDecimal quantityCoefficient;
    public BigDecimal additionalCoefficient;
    public BigDecimal bonusPoints = new BigDecimal(0);

    public BigDecimal totalPoints;
    public BigDecimal totalPointsWithCeiledQuality;
    public BigDecimal totalPointsWithQualityTwo;

    public CEESMMXVIUAMarkedArticle(
            String articleID,
            String articleName,
            String author,
            int added,
            String topic,
            int week,
            String badges,
            Boolean fromLists,
            Boolean isList,
            Boolean isDisqualified,
            double Krutyvuss,
            double VolodymyrDk,
            double Visem,
            double JurorYon
    ) {
        this.articleID = articleID;
        this.articleName = articleName;
        System.out.println("articleName = " + articleName);

        this.author = author;
        this.added = added;
        this.topic = topic;
        this.week = week;
        this.badges = badges;
        if (!badges.equals("")) {
            System.out.println("Badges: " + badges);
        }
        this.fromLists = fromLists;
        this.isList = isList;
        this.isDisqualified = isDisqualified;
        this.Krutyvuss = new BigDecimal(Krutyvuss);
        this.VolodymyrDk = new BigDecimal(VolodymyrDk);
        this.Visem = new BigDecimal(Visem);
        this.JurorYon = new BigDecimal(JurorYon);

        this.qualityCoefficient = averageMark(this.Krutyvuss, this.VolodymyrDk, this.Visem, this.JurorYon);
        System.out.println("qualityCoefficient = " + qualityCoefficient);
        this.quantityCoefficient = quantity();
        System.out.println("quantityCoefficient = " + quantityCoefficient);

        this.additionalCoefficient = new BigDecimal(fromLists ? 2 : 1);
        System.out.println("additionalCoefficient = " + additionalCoefficient);

        if (badges.contains("FA")) {
            this.bonusPoints = this.bonusPoints.add(new BigDecimal(50));
        }
        if (badges.contains("GA")) {
            this.bonusPoints = this.bonusPoints.add(new BigDecimal(25));
        }
        if (badges.contains("FL")) {
            this.bonusPoints = this.bonusPoints.add(new BigDecimal(15));
        }
        System.out.println("bonusPoints = " + bonusPoints);

        this.totalPoints = computeTotal(0);
        System.out.println("totalPoints = " + totalPoints);

        this.totalPointsWithCeiledQuality = computeTotal(1);
        System.out.println("totalPointsWithCeiledQuality = " + totalPointsWithCeiledQuality);

        this.totalPointsWithQualityTwo = computeTotal(2);
        System.out.println("totalPointsWithQualityTwo = " + totalPointsWithQualityTwo);

    }

    private BigDecimal averageMark(BigDecimal... number) {
        BigDecimal many = new BigDecimal(0);
        BigDecimal sigma = new BigDecimal(0);
        for (BigDecimal d : number) {
            if (d.compareTo(new BigDecimal(-1)) == 1) {
                many = many.add(new BigDecimal(1));
                System.out.println("I'm gonna add " + d);
                sigma = sigma.add(d);
            }
        }
        System.out.println("many: " + many + "\tsigma: " + sigma);
        return (sigma.compareTo(new BigDecimal(0)) == 0) ? new BigDecimal(0) : sigma.divide(many);
    }

    private BigDecimal quantity() {

        BigDecimal quantity = new BigDecimal(0);
        if (added >= 3500 && (isList || added < 5000)) {

            quantity = new BigDecimal(0.8);
        } else if (added >= 5000 && added < 10000) {

            quantity = new BigDecimal(1);
        } else if (added >= 10000) {

            quantity = new BigDecimal(1.1);
        }

        return quantity;
    }

    private BigDecimal computeTotal(int ceilQuality) {
        System.out.println("Size divided by 1000 = " + ((new BigDecimal(added)
                .divide(new BigDecimal(1000)))));
        BigDecimal total = ((new BigDecimal(added)
                .divide(new BigDecimal(1000)))
                .multiply(
                        (ceilQuality == 1
                        ? qualityCoefficient.setScale(0, BigDecimal.ROUND_CEILING)
                        : (ceilQuality == 2
                        ? new BigDecimal(2)
                        : qualityCoefficient))
                )
                .multiply(quantityCoefficient)
                .multiply(additionalCoefficient))
                .add(bonusPoints);
        return total;
    }

}
