/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.mmxvii;

import basebot.ceespring.out.*;
import basebot.ceespring.DatabasePart;
import java.sql.ResultSet;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class outPartUsersDetailedVII {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);
        DatabasePart db = new DatabasePart();

        String[][] wheres = {
            {
                "Users (expanded)",
                "Users (expanded)/be-taraskwiki"
            },
            {
                "",
                "WHERE `wiki` = \"be-tarask.wikipedia.org\"\n"
            },
            {
                "Raw output of data gathered. Only wikis with the template were taken into account for now.\n",
                ""
            },
            {
                "ORDER BY `articles` DESC",
                "ORDER BY `author_bytes` DESC"
            }
        };

        for (int i = 0; i < wheres[0].length; i++) {
            String page = wheres[0][i];
            String where = wheres[1][i];
            String specialComment = wheres[2][i];
            String order = wheres[3][i];

            String wrt = specialComment + "{{User:BaseBot/CEES/MMXVII/Users (expanded)/header}}\n";

            String query = "SELECT `author_name`,`wiki`,\n"
                    + "            `author_isnoob`,\n"
                    + "            COUNT(DISTINCT `article_id`) AS articles,\n"
                    + "            SUM(CASE WHEN  `article_words` >  300 THEN 1 ELSE 0 END) as articles_with_over300,\n"
                    + "            SUM(`article_bytes`) as bytes,\n"
                    + "            SUM(CASE WHEN  `article_words` >  300 THEN `article_bytes` ELSE 0 END) as bytes_over300,\n"
                    + "            SUM(CASE WHEN  `article_isperson` =  \"yes\" THEN 1 ELSE 0 END) as about_people,\n"
                    + "            SUM(CASE WHEN  `article_iswoman` =  \"yes\" THEN 1 ELSE 0 END) as about_women,\n"
                    + "            SUM(`article_authorbytessigma`) as author_bytes\n"
                    + "FROM `2017`\n"
                    + where
                    + "GROUP BY `wiki`,`author_name`\n"
                    + order;

            System.out.println(query);

            ResultSet generalOutputtie = db.generalOutputtie(query);
            int counter = 0;
            while (generalOutputtie.next()) {
                wrt += "{{User:BaseBot/CEES/MMXVII/Users (expanded)/row";
                wrt += "\n|counter                 = " + (++counter);
                String wiki = generalOutputtie.getString(2);
                wiki = wiki.substring(0, wiki.indexOf('.'));
                wrt += "\n|wiki                    = " + wiki;
                wrt += "\n|user                    = " + generalOutputtie.getString(1);
                wrt += "\n|is_new                  = " + generalOutputtie.getString(3);
                wrt += "\n|articles                = " + generalOutputtie.getInt(4);
                wrt += "\n|articles_with_over300   = " + generalOutputtie.getInt(5);
                wrt += "\n|bytes                   = " + generalOutputtie.getInt(6);
                wrt += "\n|bytes_over300           = " + generalOutputtie.getInt(7);
                wrt += "\n|bytes_by_author         = " + generalOutputtie.getInt(10);
                wrt += "\n|about_people            = " + generalOutputtie.getInt(8);
                wrt += "\n|about_women             = " + generalOutputtie.getInt(9);
                wrt += "\n}}\n\n";
            }

            wrt += "\n|}\n\n";
            m.edit("User:BaseBot/CEES/MMXVII/" + page, wrt, "creating/updating");

        }
        db.stopDB();
    }
}
