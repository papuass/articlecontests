/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring.mmxvii;

import basebot.ceespring.out.*;
import basebot.ceespring.DatabasePart;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class outPartVII {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);
        DatabasePart db = new DatabasePart();

        String[][] wheres = {
            {
                "General output",
                "Specific outputs/Wikidata unlinked",
                "Specific outputs/Listed only",
                "Specific outputs/By new users",
                "Specific outputs/Personalia",
                "Specific outputs/Improved by Misterious CEE Springman",
                //"Specific outputs/Week of Women in Science and Education/All women",
                //  "Specific outputs/Week of Women in Science and Education/Women related to Science and Education"
                "Specific outputs/Excellent"
            },
            {
                "",
                "WHERE `article_wdentity` = \"N/A\"\n",
                "WHERE `article_fromlist` = 1\n",
                "WHERE `author_isnoob` = \"yes\"\n",
                "WHERE `article_isperson` = \"yes\"\n",
                "WHERE `author_name` = \"Misterious CEE Springman\"",
                //                "WHERE `article_iswoman` = \"yes\" AND `article_isimproved` = \"no\" "
                //                + "AND (`article_creationdate` BETWEEN 20160501210000 AND 20160508205959)",
                //                "WHERE `article_iswoman` = \"yes\" AND `article_isimproved` = \"no\" "
                //                + "AND (`article_creationdate` BETWEEN 20160501210000 AND 20160508205959) "
                //                + "AND ((`article_topic` LIKE \"%Science%\") OR (`article_topic` LIKE \"%Education%\"))"
                "WHERE `article_badge` != \"\" AND  `article_badge` != \"N/A\""
            },
            {
                "",
                "",
                "",
                "Criterion of new user from ''Condensed milk via post'' "
                + "(''Wikizghushchivka'') is used at the moment:\n"
                + "* No edits in the wiki 2 month before the start, that being "
                + "before 21 February for this edition\n\n"
                + "The new user criteria are a subject to discussion, "
                + "especially those who have their own criteria for determining "
                + "local best new user are welcome to voice them\n"
                + "(I can output different lists per diffrent criteria)",
                "Please check whether the article is connected to WD entity which "
                + "has [[:d:P31]] set to [[:d:Q5]] in case of it missing here first",
                "The articles were improved. The facts were marked with talk page "
                + "templates. But we don't know who they were improved by!",
                //                "List of all articles about women created withing Week of Women 
                //                in Science and Education, CEST time was used.",
                //                "List of all articles about women related to Science or 
                //                Education created withing Week of Women in Science and Education, CEST time was used."
                "Only excellent articles (FA, GA, FL) are listed here. "
                + "Please not that they could become such before "
                + "(in case of improved articles, or after the contest timespan. "
                + "That fact is not checked by the bot."
            }
        };

        for (int i = 0; i < wheres[0].length; i++) {
            String page = wheres[0][i];
            String where = wheres[1][i];
            String specialComment = wheres[2][i];

            StringBuffer wrt = new StringBuffer();
            wrt.append("Raw output of data gathered. Only wikis with the template were taken into account for now.\n");
            wrt.append("As of the newest article here creation date and time :)\n\n");
            wrt.append(specialComment);
            wrt.append("\n\n");

            String order = "ORDER BY `2017`.`article_creationdate` ASC";
//            if (i == 6 || i == 7) {
//                order = "ORDER BY `article_words` DESC";
//            }

            String query = "SELECT * FROM `2017`\n"
                    + where
                    + order;

            System.out.println(query);

            ResultSet generalOutputtie = db.generalOutputtie(query);
            int counter = 0;
            String rows = "";

            List<String> subpages = new ArrayList<>();
            while (generalOutputtie.next()) {
                rows += "{{User:BaseBot/CEES/MMXVII/General output/row";
                rows += "\n|counter               = " + (++counter);
                String wiki = generalOutputtie.getString(2);
                wiki = wiki.substring(0, wiki.indexOf('.'));
                rows += "\n|wiki                  = " + wiki;
                rows += "\n|article_id            = " + generalOutputtie.getInt(3);
                rows += "\n|article_title         = " + generalOutputtie.getString(4);
                rows += "\n|author_id             = " + generalOutputtie.getString(5);
                rows += "\n|author_ca             = " + generalOutputtie.getString(6);
                rows += "\n|author_name           = " + generalOutputtie.getString(7);
                rows += "\n|author_gender         = " + generalOutputtie.getString(8);
                rows += "\n|author_isnew          = " + generalOutputtie.getString(9);
                rows += "\n|article_bytes         = " + generalOutputtie.getInt(10);
                rows += "\n|article_words         = " + generalOutputtie.getInt(11);
                rows += "\n|bytes_by_author       = " + generalOutputtie.getInt(21);
                rows += "\n|article_country       = " + generalOutputtie.getString(12);
                rows += "\n|article_topic         = " + generalOutputtie.getString(13);
                rows += "\n|article_isperson      = " + generalOutputtie.getString(14);
                rows += "\n|article_iswoman       = " + generalOutputtie.getString(15);
                rows += "\n|article_creationdate  = " + generalOutputtie.getTimestamp(16);
                rows += "\n|article_wdentity      = " + generalOutputtie.getString(17);
                rows += "\n|article_fromlist      = " + generalOutputtie.getString(18);
                rows += "\n|article_badge         = " + generalOutputtie.getString(25);
                rows += "\n|references            = " + "";

                rows += "\n}}\n\n";

                if (counter > 1 && counter % 1000 == 0) {
                    String subpage = "";
                    subpage += "<noinclude>\n"
                            + "{{User:BaseBot/CEES/MMXVII/General output/header}}\n"
                            + "</noinclude>";
                    subpage += rows;
                    subpage += "<noinclude>\n"
                            + "|}\n"
                            + "</noinclude>";
                    String subpagename = "User:BaseBot/CEES/MMXVII/" + page + "/" + counter / 1000;
                    m.edit(subpagename, subpage, "creating/updating");
                    subpages.add(subpagename);
                    rows = "";
                }
            }

            if (!"".equals(rows)) {
                String subpage = "";
                subpage += "<noinclude>\n"
                        + "{{User:BaseBot/CEES/MMXVII/General output/header}}\n"
                        + "</noinclude>";
                subpage += rows;
                subpage += "<noinclude>\n"
                        + "|}\n"
                        + "</noinclude>";
                String subpagename = "User:BaseBot/CEES/MMXVII/" + page + "/" + (int) Math.ceil(((float) counter) / 1000);
                m.edit(subpagename, subpage, "creating/updating");
                subpages.add(subpagename);
            }

            String[] subpagesarray = subpages.toArray(new String[subpages.size()]);
            if (subpagesarray.length > 3) {
                for (int j = 0; j < subpagesarray.length; j++) {
                    String asubpage = subpagesarray[j];
                    wrt.append("\n* [[")
                            .append(asubpage)
                            .append("]]: ")
                            .append(j * 1000)
                            .append("–")
                            .append((j + 1) * 1000);
                }
            } else {
                wrt.append("{{User:BaseBot/CEES/MMXVII/General output/header}}\n\n");
                for (String asubpage : subpagesarray) {
                    wrt.append("\n{{").append(asubpage).append("}}");
                }
                wrt.append("\n|}\n\n");
            }

            m.edit("User:BaseBot/CEES/MMXVII/" + page, wrt.toString(), "creating/updating");
            //System.out.println(wrt);
        }

        db.stopDB();

    }



}
