/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring;

import basebot.ceespring.objects.CEESTableLine;
import basebot.ceespring.objects.CEESUser;
import java.sql.*;
import java.util.Calendar;
import java.util.TimeZone;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class DatabasePart {

    public Connection conn;
    java.sql.Date startDate;
    Calendar calendar;

    public DatabasePart() throws Exception {
        // create a mysql database connection
        String myDriver = "org.gjt.mm.mysql.Driver";
        String myUrl = "jdbc:mysql://localhost/cees?characterEncoding=utf8";
        Class.forName(myDriver);
        conn = DriverManager.getConnection(myUrl, "root", "");

        // create a sql date object so we can use it in our INSERT statement
        calendar = Calendar.getInstance();
        startDate = new java.sql.Date(calendar.getTime().getTime());
    }

    public DatabasePart(String db) throws Exception {
        // create a mysql database connection
        String myDriver = "org.gjt.mm.mysql.Driver";
        String myUrl = "jdbc:mysql://localhost/" + db + "?characterEncoding=utf8";
        Class.forName(myDriver);
        conn = DriverManager.getConnection(myUrl, "root", "");

        // create a sql date object so we can use it in our INSERT statement
        calendar = Calendar.getInstance();
        startDate = new java.sql.Date(calendar.getTime().getTime());
    }

    /**
     * Truncates the table given
     *
     * @param table - name of the db table to be truncated
     * @throws SQLException
     */
    public void truncateTable(String table) throws SQLException {
        String query = "TRUNCATE `" + table + "`";
        Statement st = conn.createStatement();
        st.execute(query);
    }

    public void Inserterie(CEESTableLine row, CEESUser wikiuser) throws Exception {

        // create a mysql database connection
//        String myDriver = "org.gjt.mm.mysql.Driver";
//        String myUrl = "jdbc:mysql://localhost/cees?characterEncoding=utf8";
//        Class.forName(myDriver);
//        Connection conn = DriverManager.getConnection(myUrl, "root", "");
//        // create a sql date object so we can use it in our INSERT statement
//        Calendar calendar = Calendar.getInstance();
//        java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
        // the mysql insert statement
        String query = " insert into `2016` (wiki, article_id, article_title, "
                + "author_id, author_ca, author_name, author_gender, "
                + "author_isnoob, article_bytes, article_words, article_country,"
                + " article_topic, article_isperson, article_iswoman, "
                + "article_creationdate, article_wdentity, article_fromlist, "
                + "article_isimproved, article_fromweek, "
                + "article_authorbytessigma, article_authorbytesweeksigma, "
                + "article_authoroldid, article_authorweekoldid, article_badge, article_references)"
                + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setString(1, row.wname);
        preparedStmt.setInt(2, row.pageid);
        preparedStmt.setString(3, row.article);
        preparedStmt.setString(4, wikiuser.identifier);
        preparedStmt.setString(5, wikiuser.caid);
        preparedStmt.setString(6, row.user);
        preparedStmt.setString(7, wikiuser.gender);
        preparedStmt.setString(8, wikiuser.isNoob);
        preparedStmt.setInt(9, row.size);
        preparedStmt.setInt(10, row.words);
        preparedStmt.setString(11, row.country);
        preparedStmt.setString(12, row.topic);
        preparedStmt.setString(13, row.humanity);
        preparedStmt.setString(14, row.female);
        preparedStmt.setDate(15, row.timestamp, Calendar.getInstance(TimeZone.getTimeZone("UTC")));
        preparedStmt.setString(16, row.wdentity);
        preparedStmt.setBoolean(17, row.isfromthelist);
        preparedStmt.setString(18, row.isimproved);
        preparedStmt.setInt(19, row.inweek);
        preparedStmt.setInt(20, row.bytessigma);
        preparedStmt.setInt(21, row.weeksigma);
        preparedStmt.setInt(22, row.authoroldid);
        preparedStmt.setInt(23, row.weekoldid);
        preparedStmt.setString(24, row.badge);
        preparedStmt.setInt(25, row.references);
        

        // execute the preparedstatement
        preparedStmt.execute();

//        conn.close();
    }

    public ResultSet generalOutputtie(String query) throws Exception {
        Statement m_Statement = conn.createStatement();
        ResultSet m_ResultSet = m_Statement.executeQuery(query);
        return m_ResultSet;
    }

    /**
     * Makeshift shit for adding the column year after the contest
     *
     * @throws SQLException
     */
    public void insertReferencesNumberTable(String table, String wiki, int articleId, int number) throws SQLException {
        String query = ""
                + "UPDATE `" + table + "` \n"
                + "SET article_references=" + number + "\n"
                + "WHERE `wiki`=\"" + wiki + "\" AND article_id=" + articleId + "; ";
        Statement st = conn.createStatement();
        System.out.println(query);
        st.execute(query);
    }

    public void stopDB() throws SQLException {
        conn.close();
    }
}
