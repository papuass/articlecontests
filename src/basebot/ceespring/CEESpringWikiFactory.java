/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.ceespring;

import java.util.Map;
import objects.Credentials;
import org.wikipedia.BaseBot;

/**
 * Not a textbook implementation of a factory unfortunately, but once again I go
 * for working code rather than good code, still an improvement though I believe
 *
 * @author Base
 */
public class CEESpringWikiFactory {

    public static CEESpringWiki getWiki(
            String wikiName,
            String tlname,
            String key,
            BaseBot d,
            Map<String, Map<String, String>> countries,
            Map<String, Map<String, String>> topics,
            Credentials c
    ) {
        CEESpringWiki w;
        switch (wikiName) {
//            case "de.wikipedia.org":
//                w = new GermanWiki();
//                break;
//            case "cs.wikipedia.org":
//                w = new CzechWiki();
//                break;
//            case "ro.wikipedia.org":
//                w = new RomanianWiki();
//                break;

            default:
                w = new CEESpringWiki(wikiName, tlname, key, d, countries, topics, c);

        }

        return w;
    }
}
