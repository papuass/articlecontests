package basebot.ceespring;

import basebot.ceespring.objects.CEESTableLine;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import basebot.ceespring.objects.CEESUser;
import java.util.Arrays;
import java.util.regex.Pattern;
import javax.security.auth.login.FailedLoginException;
import org.wikipedia.BaseBot;
import org.wikipedia.Wiki;
import objects.Credentials;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class RewrittenStatisticsCollector {

    static int maincounter = 0;
    static int maincounterlim = 0;
    static Set<CEESTableLine> rows = new HashSet<>();
    static Set<String> theLists = new HashSet<>();
    static Set<String> qs = new HashSet<>(); // Wikibase entities
    static Map<String, Map<String, CEESUser>> wikisusers = new HashMap<>();

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws javax.security.auth.login.FailedLoginException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws IOException, FailedLoginException, InterruptedException {
        final Credentials c = new Credentials(args);
        BaseBot d = initialiseWiki("www.wikidata.org", c);
        // @todo investigate if I can somehow use single login for all wikis
        // SUL was made for something, wasn't it?
        BaseBot m = initialiseWiki("meta.wikimedia.org", c);

        fetchListedArticlesEntities(m, d);
        Map<String, String> participatingWikis = fetchTemplateUsingWikis(d);
        Set<String> keySet = participatingWikis.keySet();

        //Adding fancy wikis here for simplicity.
        Set<String> wikiDatabases = new HashSet<>();
        wikiDatabases.addAll(keySet);
        wikiDatabases.add("cswiki");//Education programme extension
        wikiDatabases.add("dewiki");//Manually maintained list
        String[] keys = wikiDatabases.toArray(new String[wikiDatabases.size()]);

        maincounterlim = keys.length;
        System.out.println("maincounterlim = " + maincounterlim);
        ExecutorService service = Executors.newCachedThreadPool();
        final Map<String, Map<String, String>> topics = populateTopics(d);
        final Map<String, Map<String, String>> countries = populateCountries(d);

        for (int i = 0; i < keys.length; i++) {
            String key = keys[i];
            final int threadcounter = i;
            if (!key.matches("[_a-z]+wiki")) {
                maincounter++;
                continue;
            }

            String wikiName = key.substring(0, key.indexOf("wiki")).replaceAll("_", "-") + ".wikipedia.org";
            String tlname = participatingWikis.containsKey(key) ? participatingWikis.get(key) : "";

            if ("be-x-old.wikipedia.org".equals(wikiName)) {
                wikiName = "be-tarask.wikipedia.org";
            }

            final String wname_f = wikiName;
            final String tlname_f = tlname;
            final String key_f = key;
            final BaseBot d_f = d;

            final CEESpringWiki wiki = CEESpringWikiFactory.getWiki(wikiName, tlname, key, d, countries, topics, c);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Thread.currentThread().setName(wname_f);
                    try {
                        workInWiki(threadcounter, wiki, c);
                    } catch (Exception ex) {
                        Logger.getLogger(StatisticsCollector.class.getName()).log(Level.SEVERE, null, ex);
                        System.exit(666);
                    }
                }
            };
            Thread.sleep(5000);
            Future<?> submit = service.submit(runnable);
        }
    }

    /**
     * Creates a BaseBot object of a wiki given, logs in there and sets up mark
     * bot and minor booleans
     *
     * @param wiki
     * @param c
     * @return
     * @throws IOException
     * @throws FailedLoginException
     */
    public static BaseBot initialiseWiki(String wiki, Credentials c)
            throws IOException, FailedLoginException {
        BaseBot w = new BaseBot(wiki);
        w.setUserAgent("BaseBot");
        w.login(c.wikilogin, c.wikipassword);
        w.setMarkBot(true);
        w.setMarkMinor(true);
        return w;
    }

    public static void workInWiki(
            int thread, CEESpringWiki wiki, Credentials c) throws Exception {

        String wikiName = wiki.wikiName;
        String wdwiki = wiki.wikiWikidataName;
        String tlname = wiki.templateName;
        Map<String, Map<String, String>> topics = CEESpringWiki.topics;
        Map<String, Map<String, String>> countries = CEESpringWiki.countries;
        BaseBot d = wiki.d;
        
        System.out.println(wiki.wikiName + "\t" + wiki.templateName + "\t" + Thread.currentThread().getName());

        BaseBot w = initialiseWiki(wikiName, c);
        String[] talkPages;

        // true for wikis with the source of data other than the template
        // it makes no sense to try to parse talkpage for them
        boolean templatePhobic = false;
        Map<String, String> labsrepo = null;
        Map<String, String> templatePhobicArticles = new HashMap<>();
        switch (wikiName) {
            case "ro.wikipedia.org":
                talkPages = w.getCategoryMembers("Articole participante la Wikimedia CEE Spring 2016", 1);
                break;
            case "cs.wikipedia.org":
                templatePhobic = true;
                String[] courses = {
                    "Soutěže/CEE Spring - Mezinárodní kategorie (2016)",
                    "Soutěže/CEE Spring - Polská města (2016)",
                    "Soutěže/CEE Spring - Československý odboj za první světové války (2016)"
                };
                ReplicationFetcher courseFetcher = new ReplicationFetcher(c);
                labsrepo = courseFetcher.fetchCoursesArticles(wdwiki, courses);
                Set<String> cswiki_talks = labsrepo.keySet();
                talkPages = cswiki_talks.toArray(new String[cswiki_talks.size()]);
                break;
            case "de.wikipedia.org":
                templatePhobic = true;
                // @todo move to a method
                String dewikiResultsPage = "Wikipedia:Wikimedia CEE Spring 2016";
                String dewikiResultsPageText = w.getPageText(dewikiResultsPage);
                String resultsTableStartMark = "<!-- Results table -->";
                String resultsTableEndMark = "<!-- /Results table -->";
                int resultsTableStartIndex = dewikiResultsPageText.indexOf(resultsTableStartMark) + resultsTableStartMark.length();
                int resultsTableEndIndex = dewikiResultsPageText.indexOf(resultsTableEndMark, resultsTableStartIndex);
                if (resultsTableStartIndex == -1 || resultsTableEndIndex == -1) {
                    return;
                }
                String resultsTable = dewikiResultsPageText.substring(
                        resultsTableStartIndex,
                        resultsTableEndIndex
                );
                String[] resultsTableRows = resultsTable.split("\n\\|-");
                for (int i = 1; i < resultsTableRows.length; i++) {
                    String resultsTableRow = resultsTableRows[i];
                    String[] resultsTableCell = resultsTableRow.split("\n\\|\\s");
                    String resultsTableCellAuthor = resultsTableCell[0];
                    Pattern pat = Pattern.compile(".*\\[\\[(?:[Bb]enutzer|[Uu]ser):(.*)\\|.*]].*");
                    String parsedAuthor;
                    try {
                        parsedAuthor = pat.matcher(resultsTableCellAuthor).group(1).trim();
                    } catch (java.lang.IllegalStateException ex) {
                        // java.lang.IllegalStateException: No match found
                        // TODO: investigate wtf is this happening
                        continue;
                    }
                    String resultsTableCellArticles = resultsTableCell[1];
                    String[] split = resultsTableCellArticles.split("]], \\[\\[");
                    for (String articleInCell : split) {
                        articleInCell = articleInCell.replaceFirst("\\[\\[", "").trim();
                        templatePhobicArticles.put(articleInCell, parsedAuthor);
                    }
                }
                talkPages = templatePhobicArticles.keySet().toArray(new String[templatePhobicArticles.keySet().size()]);
                break;
            default:
                talkPages = w.whatTranscludesHere(tlname, 1);
                break;
        }

        boolean amIBot = w.getUser(c.wikilogin).isA("bot");
        int batchlim = amIBot ? 450 : 45;
//        ReplicationFetcher articlesDataFetcher = new ReplicationFetcher(args);
//        Map<String, CEESUser> articlesData = articlesDataFetcher.fetchArticlesData(3366 + 100 + thread, wdwiki, talkpages);
        Map<String, String> pageTexts = fetchPageTexts(batchlim, w, talkPages);

        //TOPICS
        Set<String> topicNamesSet = topics.keySet();
        String[] topicNames = topicNamesSet.toArray(new String[topicNamesSet.size()]);
        Map<String, Set<String>> topicTalks = new HashMap<>();

        topicsiterator:
        for (String topicname : topicNames) {
            if (!topics.get(topicname).containsKey(wdwiki)) {
                System.out.println("There are is " + topicname + " in " + wdwiki);
                continue topicsiterator;
            }
            String topiccat = topics.get(topicname).get(wdwiki);
            System.out.println(wdwiki + "\t" + topiccat);
            String[] topicedtalks = w.getCategoryMembers(topiccat, 1);
            Set<String> talks = new HashSet<>();
            topicedtalksiterator:
            talks.addAll(Arrays.asList(topicedtalks));
            topicTalks.put(topicname, talks);
        } //END TOPICS

        //COUNTRIES
        Set<String> countryNamesSet = countries.keySet();
        String[] countryNames = countryNamesSet.toArray(new String[countryNamesSet.size()]);
        Map<String, Set<String>> countryTalks = new HashMap<>();

        countriesiterator:
        for (String countryname : countryNames) {
            if (!countries.get(countryname).containsKey(wdwiki)) {
                System.out.println("There are no " + countryname + "-articles in " + wdwiki);
                continue countriesiterator;
            }
            String countrycat = countries.get(countryname).get(wdwiki);
            System.out.println(wdwiki + "\t" + countrycat);
            String[] countriedtalks = w.getCategoryMembers(countrycat, 1);
            Set<String> talks = new HashSet<>();
            countriedtalksiterator:
            talks.addAll(Arrays.asList(countriedtalks));
            countryTalks.put(countryname, talks);
        } //END COUNTRIES

        Map<String, CEESUser> cashedUsers = new HashMap<>();
        Set<String> usersToFetch = new HashSet<>();

        talkpageiteration:
        for (String talkPage : talkPages) {
            String article = talkPage.substring(talkPage.indexOf(":") + 1);
            Map pageInfo = w.getPageInfo(article);
            long pageidentifier = (long) pageInfo.get("pageid");
            int pageid = Integer.parseInt(pageidentifier + "");//page id which we need
            int size = (int) pageInfo.get("size");// page size in bytes which we need
            Wiki.Revision[] pageHistory = w.getPageHistory(article);
            if (pageHistory.length == 0) {
                continue;
            }
            Wiki.Revision last = pageHistory[pageHistory.length - 1];
            Calendar timestamp = last.getTimestamp(); // page creation date which we need
            timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));

            Calendar contestStart;
            Calendar contestEnd;
            Calendar[][] contestWeeks = null;
            Calendar week1_end = null;
            Calendar week2_start = null;
            Calendar week2_end = null;
            Calendar week3_start = null;
            Calendar week3_end = null;
            Calendar week4_start = null;
            Calendar week4_end = null;
            Calendar week5_start = null;
            Calendar week5_end = null;
            Calendar week6_start = null;
            Calendar week6_end = null;
            Calendar week7_start = null;
            Calendar week7_end = null;
            Calendar week8_start = null;
            Calendar week8_end = null;
            Calendar week9_start = null;
            Calendar week9_end = null;
            Calendar week10_start = null;
            Calendar week10_end = null;
            Calendar week11_start = null;
            Calendar week11_end = null;
            switch (wikiName) {
                //perfectly should be moved to some config
                //all the dates besides the very first should be calculated
                //basing on timezone. DST sucks.
                case "az.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 31,
                            20, 00, 00);//Azerbaijan, UTC+4
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            19, 59, 59);//Azerbaijan, UTC+4
                    break;
                case "ba.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            19, 00, 00);//Bashkortostan, UTC+5
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            18, 59, 59);//Bashkortostan, UTC+5
                    break;
                case "be.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Belarus, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Belarus, UTC+3
                    break;
                case "be-tarask.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Belarus, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Belarus, UTC+3
                    break;
                case "bg.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Bulgaria, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Bulgaria, UTC+2
                    break;
                case "cs.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//The Czech Republic, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            22, 59, 59);//The Czech Republic, UTC+1
                    break;
                case "de.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Austria, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Austria, UTC+3
                    break;
                case "eo.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            24, 00, 00);//Esperantujo, UTC
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            23, 59, 59);//Esperantujo, UTC
                    break;
                case "el.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Greece, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Greece, UTC+3
                    break;
                case "et.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Estonia, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Estonia, UTC+2
                    break;
                case "hu.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Hungary, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Hungary, UTC+3
                    break;
                case "ka.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            20, 00, 00);//Georgia, UTC+4
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            19, 59, 59);//Georgia, UTC+4
                    break;
                case "lt.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Lithuania, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Lithuania, UTC+3
                    break;
                case "lv.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Latvia, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Latvia, UTC+3
                    break;
                case "mk.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Macedonia, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            22, 59, 59);//Macedonia, UTC+1
                    break;
                case "pl.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Polonia, UTC+1
                    week1_end = new GregorianCalendar(2016, 2, 27,
                            21, 59, 59);
                    week2_start = new GregorianCalendar(2016, 2, 27,
                            22, 00, 00);
                    week2_end = new GregorianCalendar(2016, 3, 03,
                            21, 59, 59);
                    week3_start = new GregorianCalendar(2016, 3, 03,
                            22, 00, 00);
                    week3_end = new GregorianCalendar(2016, 3, 10,
                            21, 59, 59);
                    week4_start = new GregorianCalendar(2016, 3, 10,
                            22, 00, 00);
                    week4_end = new GregorianCalendar(2016, 3, 17,
                            21, 59, 59);
                    week5_start = new GregorianCalendar(2016, 3, 17,
                            22, 00, 00);
                    week5_end = new GregorianCalendar(2016, 3, 24,
                            21, 59, 59);
                    week6_start = new GregorianCalendar(2016, 3, 24,
                            22, 00, 00);
                    week6_end = new GregorianCalendar(2016, 4, 1,
                            21, 59, 59);
                    week7_start = new GregorianCalendar(2016, 4, 1,
                            22, 00, 00);
                    week7_end = new GregorianCalendar(2016, 4, 8,
                            21, 59, 59);
                    week8_start = new GregorianCalendar(2016, 4, 8,
                            22, 00, 00);
                    week8_end = new GregorianCalendar(2016, 4, 15,
                            21, 59, 59);
                    week9_start = new GregorianCalendar(2016, 4, 15,
                            22, 00, 00);
                    week9_end = new GregorianCalendar(2016, 4, 22,
                            21, 59, 59);
                    week10_start = new GregorianCalendar(2016, 4, 22,
                            22, 00, 00);
                    week10_end = new GregorianCalendar(2016, 4, 29,
                            21, 59, 59);
                    week11_start = new GregorianCalendar(2016, 4, 29,
                            22, 00, 00);
                    week11_end = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Polonia, UTC+1
                    break;
                case "ro.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Romania, Moldova, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Romania, Moldova, UTC+2
                    break;
                case "ru.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            21, 00, 00);//Russia, UTC+3
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Russia, UTC+3
                    break;
                case "sk.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Slovakia, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Slovakia, UTC+2
                    break;
                case "sq.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Albania, Kosovo, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            22, 59, 59);//Albania, Kosovo, UTC+1
                    break;
                case "sr.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            23, 00, 00);//Serbia, Srpska, UTC+1
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Serbia, Srpska, UTC+2
                    break;
                case "tr.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Turkey, UTC+2
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            21, 59, 59);//Turkey, UTC+2
                    break;
                case "uk.wikipedia.org":
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            22, 00, 00);//Ukraine, UTC+2
                    week1_end = new GregorianCalendar(2016, 2, 27,
                            20, 59, 59);
                    week2_start = new GregorianCalendar(2016, 2, 27,
                            21, 00, 00);
                    week2_end = new GregorianCalendar(2016, 3, 03,
                            20, 59, 59);
                    week3_start = new GregorianCalendar(2016, 3, 03,
                            21, 00, 00);
                    week3_end = new GregorianCalendar(2016, 3, 10,
                            20, 59, 59);
                    week4_start = new GregorianCalendar(2016, 3, 10,
                            21, 00, 00);
                    week4_end = new GregorianCalendar(2016, 3, 17,
                            20, 59, 59);
                    week5_start = new GregorianCalendar(2016, 3, 17,
                            21, 00, 00);
                    week5_end = new GregorianCalendar(2016, 3, 24,
                            20, 59, 59);
                    week6_start = new GregorianCalendar(2016, 3, 24,
                            21, 00, 00);
                    week6_end = new GregorianCalendar(2016, 4, 1,
                            20, 59, 59);
                    week7_start = new GregorianCalendar(2016, 4, 1,
                            21, 00, 00);
                    week7_end = new GregorianCalendar(2016, 4, 8,
                            20, 59, 59);
                    week8_start = new GregorianCalendar(2016, 4, 8,
                            21, 00, 00);
                    week8_end = new GregorianCalendar(2016, 4, 15,
                            20, 59, 59);
                    week9_start = new GregorianCalendar(2016, 4, 15,
                            21, 00, 00);
                    week9_end = new GregorianCalendar(2016, 4, 22,
                            20, 59, 59);
                    week10_start = new GregorianCalendar(2016, 4, 22,
                            21, 00, 00);
                    week10_end = new GregorianCalendar(2016, 4, 29,
                            20, 59, 59);
                    week11_start = new GregorianCalendar(2016, 4, 29,
                            21, 00, 00);
                    week11_end = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);
                    contestEnd = new GregorianCalendar(2016, 4, 31,
                            20, 59, 59);//Ukraine, UTC+3
                    break;
                default:
                    contestStart = new GregorianCalendar(2016, 2, 20,
                            00, 00, 00);//Globe, UTC-24;
                    contestEnd = new GregorianCalendar(2016, 5, 1,
                            23, 59, 59);//Globe, UTC+24;
            }
            contestStart.setTimeZone(TimeZone.getTimeZone("UTC"));
            int startOffset = timestamp.compareTo(contestStart);
            int endoff = timestamp.compareTo(contestEnd);

            if (endoff > 0) {//the page was created after the contest start -- nothing to do here
                continue talkpageiteration;
            }
            int inWeek = 0;

            String isImproved = "no";

            boolean wikiConsidersWeeks = "uk.wikipedia.org".equals(wikiName)
                    || "pl.wikipedia.org".equals(wikiName);
            if (wikiConsidersWeeks) {
                Calendar[] starts = {contestStart, week2_start, week3_start,
                    week4_start, week5_start, week6_start, week7_start,
                    week8_start, week9_start, week10_start, week11_start};
                Calendar[] ends = {week1_end, week2_end, week3_end, week4_end,
                    week5_end, week6_end, week7_end, week8_end, week9_end,
                    week10_end, week11_end};

                Calendar[][] weeksesies = {starts, ends};
                contestWeeks = weeksesies;
            }

            if (startOffset < 0) {
                isImproved = "yes";
            } else if (wikiConsidersWeeks) {
                inWeek = getContestWeek(timestamp, contestWeeks);
            }

            System.out.println("Startoff = " + startOffset);
//            int year = timestamp.get(Calendar.YEAR);
//            int month = timestamp.get(Calendar.MONTH) + 1;
//            int day = timestamp.get(Calendar.DAY_OF_MONTH);
//            int hour = timestamp.get(Calendar.HOUR_OF_DAY);
//            int minute = timestamp.get(Calendar.MINUTE);
//            int second = timestamp.get(Calendar.SECOND);
            boolean improverless = false;
            String improver = "Misterious CEE Springman";

            if (startOffset < 0 && !templatePhobic) {
//                System.out.println("Hey this one was created before the start! "
//                        + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
                improverless = true;

                boolean articleTextsContainArticle = pageTexts.containsKey(talkPage);
                String talkPageTemplate = "";
                if (articleTextsContainArticle) {
                    talkPageTemplate = pageTexts.get(talkPage);
                    System.out.println("We've just got a\t" + wikiName + "\t" + talkPage + "\ttext from the batch variable.");
                } else {
                    talkPageTemplate = w.getPageText(talkPage);
                }
                String regex = "";//returnWikiRegexp(wikiName);

                if (!"".equals(regex) && talkPageTemplate.matches(regex)) {
                    String improvercandidate = talkPageTemplate.replaceAll(regex, "$2").trim().replaceAll("_", " ");
                    if (!"".equals(improvercandidate)) {
                        improver = improvercandidate;
                        improverless = false;
                    }
                    System.out.println("Okay it's a " + wikiName + " improved article «" + article + "», the improver seem to be " + improver);
                } else {
                    System.out.println("No regex match for talk of\t" + article);
                }
            }

//            System.out.println("The time of first revision is: " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
            String user = templatePhobic
                    ? labsrepo.get("Talk:" + article)
                    : (((startOffset < 0) && (!improverless))
                            ? improver
                            : last.getUser().trim()); // username which we need

            String gender = "";
            String userid = "";
            String caid = "";
            String isnoob = "";
            if (user.matches("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z") || improverless) {
//                gender = "N/A";
//                userid = "N/A";
//                caid = "N/A";
//                isnoob = "N/A";
                CEESUser ceesUser = new CEESUser(user);
                System.out.println("Talk pager:\t" + ceesUser);
                cashedUsers.put(user, ceesUser);
            } else if (cashedUsers.containsKey(user)) {
//                    System.out.println("The cashing users thing actually works. At least for " + user);
//                    CEESUser cashedUser = cashedUsers.get(user);
//                    gender = cashedUser.gender;
//                    userid = cashedUser.identifier;
//                    caid = cashedUser.caid;
//                    isnoob = cashedUser.isNoob;

            } else {// API way should be uncommented into some method and used when replication lag is too high
//                    Wiki.User user1 = w.getUser(user);
//
//                    Map<String, Object> userInfo = user1.getUserInfo();
//                    if (userInfo.containsKey("gender")) {
//                        gender = ((Wiki.Gender) userInfo.get("gender")).toString(); // user gender which we need
//                    }
//                    if (userInfo.containsKey("userid")) {
//                        userid = (String) userInfo.get("userid"); // user id which we need
//                    }
//                    caid = w.CAId(user) + "";
//                    Calendar c = new GregorianCalendar(2016, 0, 21);
//                    c.set(2016, 0, 21);
//                    Wiki.Revision[] contribs = w.contribs(user, "", null, c, true);
//                    int noobind = contribs.length;
//                    isnoob = noobind <= 0 ? "yes" : "no"; //is noob? which we need
//
//                    cashedUsers.put(user, new CEESUser(userid, caid, user, gender, isnoob));
                usersToFetch.add(user);
            }
            boolean articleTextesContainArticle = pageTexts.containsKey(article);
            String pt = "";
            if (articleTextesContainArticle) {
                pt = pageTexts.get(article);
                System.out.println("We've just got a\t" + wikiName + "\t" + article + "\ttext from the batch variable.");
            } else {
                System.out.println("Fetching " + wikiName + "\t" + article + "\ttext from via API.");
                pt = w.getPageText(article);
            }
            int words = countArticleWords(pt);
            int references = countArticleReferences(pt);
            int bytessigma = 0;
            int weeksigma = 0;
            int weekoldid = 0;
            int authoroldid = 0;

            boolean hasvalidrevisions = false;
            Set<String> revhashes = new HashSet<>();

            revisioniteration:
            for (int j = pageHistory.length - 1; j >= 0; j--) {
                Wiki.Revision revision = pageHistory[j];
                String revuser = revision.getUser();
                int sizeDiff = revision.getSizeDiff();
                //System.out.println("We are listing revisions of the article " + article + " now we are on #" + j + " and the diff is " + sizeDiff);
                Calendar revisionTime = revision.getTimestamp();
                //System.out.println("revuser\t" + revuser + "user\t" + user);
                String hashCode = revision.getSha1();
                boolean alreadypresentrev = revhashes.contains(hashCode);
                revhashes.add(hashCode);

                if (revuser == null) {
                    continue revisioniteration;
                }
                if (revuser.equals(user)
                        && revisionTime.compareTo(contestStart) >= 0
                        && !alreadypresentrev
                        && (!(revisionTime.compareTo(contestEnd) > 0))) {
                    hasvalidrevisions = true;
                    bytessigma += sizeDiff;
                    authoroldid = (int) revision.getRevid();

                    //<editor-fold desc="crazywiki week selection">
                    if (wikiConsidersWeeks) {
                        if (startOffset < 0 && inWeek == 0) {
                            inWeek = getContestWeek(revisionTime, contestWeeks);
                        }
                        if (inWeek > 0) {
                            if (revisionTime.compareTo(contestWeeks[0][inWeek - 1]) >= 0 && revisionTime.compareTo(contestWeeks[1][inWeek - 1]) <= 0) {
                                weeksigma += sizeDiff;
                                weekoldid = 0;
                                weekoldid = (int) revision.getRevid();
                                //System.out.println(article + "\t" + j + "\t" + sizeDiff + "\t" + weekoldid);
                            }
                        }
                    }
                    //end of crazywiki if </editor-fold>
                }
            }

            if (!hasvalidrevisions) {
                System.out.println(article + "\thas no valid revisions");
                continue talkpageiteration;
            }

            final int weekolddamndfuckingshit = weekoldid;
            String wdentity;
            String humanity;
            String female;
            String badge;
            String[] WDEntityIsHuman = d.WDEntityIsHuman(article, wdwiki);
            if (WDEntityIsHuman == null) {
                wdentity = "N/A";
                humanity = "N/A";
                female = "N/A";
                badge = "N/A";
            } else {
                wdentity = WDEntityIsHuman[0];
                humanity = WDEntityIsHuman[1];
                female = WDEntityIsHuman[2];
                switch (WDEntityIsHuman[3]) {
                    case "Q17437796":
                        badge = "FA";
                        break;
                    case "Q17437798":
                        badge = "GA";
                        break;
                    case "Q17506997":
                        badge = "FL";
                        break;
                    default:
                        badge = "";
                        break;
                }

            }

            boolean isfromthelist = theLists.contains(wdentity);
            System.out.println(article + "\t" + weekolddamndfuckingshit + "\tfrom lists:\t" + isfromthelist);

            String article_country = "";
            String article_topic = "";

            for (String topicname : topicNames) {
                if (topicTalks.containsKey(topicname)) {
                    if (topicTalks.get(topicname).contains(talkPage)) {
                        article_topic += ("".equals(article_topic) ? "" : ", ") + topicname;
                    }
                }
            }
            System.out.println(article + "\t" + article_topic);

            for (String countryname : countryNames) {
                if (countryTalks.containsKey(countryname)) {
                    if (countryTalks.get(countryname).contains(talkPage)) {
                        article_country += ("".equals(article_country) ? "" : ", ") + countryname;
                    }
                }
            }
            System.out.println(article + "\t" + article_country);

            rows.add(new CEESTableLine(wikiName, pageid, article,
                    /*userid, caid,*/ user, /*gender,
        isnoob,*/ size, words, references,
                    article_country, article_topic,
                    humanity, female, new java.sql.Timestamp(timestamp.getTime().getTime()),
                    wdentity, isfromthelist, isImproved, inWeek, bytessigma,
                    weeksigma, authoroldid, weekolddamndfuckingshit, badge
            ));
        } // end of going through talk pages

        String[] usersToFetchArray = usersToFetch.toArray(new String[usersToFetch.size()]);
        ReplicationFetcher cf = new ReplicationFetcher(c);
        //fetchUserData(String wikidb, String starttime, String noobtime, String... users)
        Map<String, CEESUser> usersData = cf.fetchUsersData(
                3366 + 1 + thread, wdwiki,
                "20160321000000",// hardcode
                "20160121000000",// hardcode
                usersToFetchArray
        );
        cashedUsers.putAll(usersData);

        wikisusers.put(wikiName, cashedUsers);

        maincounter++;
        System.out.println(maincounter + "\t" + wikiName);
        if (maincounter == maincounterlim) {
            writeToDatabase();
        }
    }//end of work in wiki

    /**
     * Writes all the stuff we fetched to our localhost database so that we can
     * proceed data later for the outputs. Reads all the stuff off global
     * variables, thus no input params are used.
     *
     * @throws Exception
     */
    public static void writeToDatabase() throws Exception {
        CEESTableLine[] rowses = rows.toArray(new CEESTableLine[rows.size()]);
        System.out.println("There are " + rowses.length + " rows in total");
        DatabasePart db = new DatabasePart();
        db.truncateTable("2016");
        for (int i = 0; i < rowses.length; i++) {
            CEESTableLine row = rowses[i];
            System.out.println(i + "\t-\tWriting to DB");
            Map<String, CEESUser> wikiusers = wikisusers.get(row.wname);
            System.out.println(row.article + "\t" + row.user);

            CEESUser wikiuser = wikiusers.containsKey(row.user) ? wikiusers.get(row.user) : new CEESUser(row.user);
            System.out.println(wikiuser.toString());
            db.Inserterie(row, wikiuser);
        }
    }

    /**
     * Resolves redirects for Wikibase repository (Wikidata) entities
     *
     * @param d - BaseBot instance for Wikibase repository (Wikidata)
     * @throws IOException
     */
    public static void resolveEntityRedirects(BaseBot d) throws IOException {
        String[] qses = qs.toArray(new String[qs.size()]);
        qs.clear();
        String query = "";
        for (String string : qses) {
            query += "".equals(query) ? string : "|" + string;
        }
        String[] qids = d.WDEntity(query.split("\\|"));
        theLists.addAll(Arrays.asList(qids));
        //System.out.println(j + "\t" + string);
    }

    /**
     * Fetches texts for all pages connected to page talks given by batches of
     * limit given or the rest
     *
     * @param lim - limitation of number of articles texts to be fetched of per
     * batch
     * @param w - BaseBot instance to work with
     * @param talks - list of page talks to fetch connected pages texts of
     * @return A map with keys for page titles and values for page texts
     * @throws Exception
     *
     * @todo Investigate why this method doesn't return <em>all</em> the page
     * texts
     */
    public static Map<String, String> fetchPageTexts(int lim, BaseBot w, String... talks) throws Exception {
        String batchurlpart = "";
        Map<String, String> pageTexts = new HashMap<>();
        int j = 0;
        for (int i = 0; i < talks.length; i++) {
            String talk = talks[i];
            String article = talk.substring(talk.indexOf(":") + 1);
            batchurlpart += "".equals(batchurlpart) ? article : "|" + article;
            if (++j == lim || i == talks.length - 1 /*|| URLEncoder.encode(batchurlpart, "UTF-8").length() > 600*/) {
                pageTexts.putAll(w.batchPageTexts(batchurlpart));
                batchurlpart = "";
            }
        }
        j = 0;
        for (int i = 0; i < talks.length; i++) {
            String talk = talks[i];
            batchurlpart += "".equals(batchurlpart) ? talk : "|" + talk;
            if (++j == lim || i == talks.length - 1 || URLEncoder.encode(batchurlpart, "UTF-8").length() > 600) {
                pageTexts.putAll(w.batchPageTexts(batchurlpart));
                batchurlpart = "";
            }
        }
        return pageTexts;
    }

    public static Map<String, Map<String, String>> populateTopics(BaseBot d) throws IOException {
        final Map<String, Map<String, String>> topics = new HashMap<>();
        topics.put("Culture", d.getSiteLinks("Q23536919"));
        topics.put("Earth", d.getSiteLinks("Q23531225"));
        topics.put("Economics", d.getSiteLinks("Q23470319"));
        topics.put("Society", d.getSiteLinks("Q23541379"));
        topics.put("Sports", d.getSiteLinks("Q23640309"));
        topics.put("Politics", d.getSiteLinks("Q23541381"));
        topics.put("Transport", d.getSiteLinks("Q23640610"));
        topics.put("History", d.getSiteLinks("Q23640619"));
        topics.put("Science", d.getSiteLinks("Q23541376"));
        topics.put("Education", d.getSiteLinks("Q23640622"));
        topics.put("Women", d.getSiteLinks("Q23640631"));
        return topics;
    }

    public static Map<String, Map<String, String>> populateCountries(BaseBot d) throws IOException {
        final Map<String, Map<String, String>> countries = new HashMap<>();
        countries.put("Albania", d.getSiteLinks("Q23728001"));
        countries.put("Armenia", d.getSiteLinks("Q23649574"));
        countries.put("Austria", d.getSiteLinks("Q23437429"));
        countries.put("Azerbaijan", d.getSiteLinks("Q23649562"));
        countries.put("Republic of Bashkortostan", d.getSiteLinks("Q23728012"));
        countries.put("Belarus", d.getSiteLinks("Q23647470"));
        countries.put("Bosnia and Herzegovina", d.getSiteLinks("Q23714165"));
        countries.put("Bulgaria", d.getSiteLinks("Q23649555"));
        countries.put("Croatia", d.getSiteLinks("Q23649747"));
        countries.put("Czech Republic", d.getSiteLinks("Q23649578"));
        countries.put("Esperantujo", d.getSiteLinks("Q23649748"));
        countries.put("Estonia", d.getSiteLinks("Q23648998"));
        countries.put("Georgia", d.getSiteLinks("Q23649750"));
        countries.put("Greece", d.getSiteLinks("Q23649563"));
        countries.put("Hungary", d.getSiteLinks("Q23437308"));
        countries.put("Kazakhstan", d.getSiteLinks("Q23714164"));
        countries.put("Kosovo", d.getSiteLinks("Q23712690"));
        countries.put("Latvia", d.getSiteLinks("Q23647476"));
        countries.put("Lithuania", d.getSiteLinks("Q23647474"));
        countries.put("Macedonia", d.getSiteLinks("Q23649564"));
        countries.put("Moldova", d.getSiteLinks("Q23649554"));
        countries.put("Poland", d.getSiteLinks("Q23649565"));
        countries.put("Romania", d.getSiteLinks("Q23647481"));
        countries.put("Russia", d.getSiteLinks("Q23647479"));
        countries.put("Sakha Republic", d.getSiteLinks("Q23714163"));
        countries.put("Republika Srpska", d.getSiteLinks("Q23649577"));
        countries.put("Serbia", d.getSiteLinks("Q23538283"));
        countries.put("Slovakia", d.getSiteLinks("Q23437381"));
        countries.put("Turkey", d.getSiteLinks("Q23649760"));
        countries.put("Ukraine", d.getSiteLinks("Q23498134"));
        return countries;
    }

    static int countArticleWords(String pageText) {
        return pageText.split("[\\p{Po}\\p{Z}\\p{S}\\p{C}]+").length;
    }

    static int countArticleReferences(String pageText) {
        return pageText.split("<ref\\s").length - 1;
    }

    static int getContestWeek(Calendar revisionTime, Calendar[][] contestWeeks) {
        int inWeek = 0;
        for (int i = 0; i <= 10; i++) {
            if (revisionTime.compareTo(contestWeeks[0][i]) >= 0 && revisionTime.compareTo(contestWeeks[1][i]) <= 0) {
                inWeek = i + 1;
                return inWeek;
            }
        }
        return inWeek;
    }

    static void fetchListedArticlesEntities(BaseBot m, BaseBot d) throws IOException {
        String[] lists = m.listPages("Wikimedia CEE Spring 2016/Structure/", null, 0);
        for (String list : lists) {
            System.out.println(list);
            if ("Wikimedia CEE Spring 2016/Structure/Statistics".equals(list)) {
                continue;
            }

            String listText = m.getPageText(list);
            boolean cont = true;
            qs.clear();

            do {
                int a = listText.indexOf("{{#invoke:WikimediaCEETable");
                if (a > -1) {
                    int b = listText.indexOf("}}", a);
                    String tl = listText.substring(a, b);
                    listText = listText.substring(b);
                    String[] split = tl.split("\\|Q");

                    for (int j = 1; j < split.length; j++) {
                        String q = split[j];
                        q = "Q" + q.replaceAll("[^\\d]+", "");
                        qs.add(q);
                        if (qs.size() >= 400) {
                            resolveEntityRedirects(d);
                        }
                    }
                } else {
                    cont = false;
                    resolveEntityRedirects(d);
                }
            } while (cont);
        }
    }

    static Map<String, String> fetchTemplateUsingWikis(BaseBot d) throws IOException {
        return d.getSiteLinks("Q22927112");
    }
    
    
    

}//Class end
