/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.physcontest;

import basebot.ceespring.*;
import java.sql.ResultSet;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class PhysContestOut {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.setUserAgent("BaseBot");
        w.login(args[0], args[1]);

        DatabasePart db = new DatabasePart("physcontest");

        String query = "SELECT *\n"
                + "FROM `2016`\n"
                + "ORDER BY `article_size` DESC";
        System.out.println(query);

        ResultSet generalOutputtie = db.generalOutputtie(query);

        String wrt = "{| class=\"wikitable sortable\"\n"
                + "! Лічильник\n"
                + "! Стаття\n"
                + "! Автор\n"
                + "! Розмір на кінець\n"
                + "! Розмір на початок\n"
                + "! Група\n";

//| [[Потребител:Drzewianin]]||нова||3||16426||1||0
//: [[:m:User:BaseBot/CEES/MMXVI/Wikis|Автоматично обновяващ се списък на броя статии, статии от списъците, статии за хора, статии за жени, автори, авторки, опитни редактори, нови редактори]]
//: Към {{subst:time|d F в H:i T}}: 120 статии, 46 от списъците, 50 за хора, от тях 37 за жени, 12 участващи автори, от тях 3 жени (според това, което са споменали в настройките си), 11 опитни редактори, 1 нов редактор.
//        
        int counter = 0;
        while (generalOutputtie.next()) {

            wrt += "|-\n";
            wrt += "|" + (++counter) + "\n";
            //лічильник 1
            wrt += "| [[" + generalOutputtie.getString(2) + "]]\n";
            //ідентифікатор 3
            wrt += "| [[Користувач:" + generalOutputtie.getString(4) + "|]]\n";
            //ідентифікатор 5
            wrt += "| " + generalOutputtie.getString(6) + "\n";
            wrt += "| " + generalOutputtie.getString(7) + "\n";
            wrt += "| " + generalOutputtie.getString(8) + "\n";
        }

        wrt += "|}";
        String page = "Вікіпедія:WikiPhysContest-2016/Конкурсні статті";
        System.out.println(wrt);
        String sectionText = w.getSectionText(page, 0);
        w.edit(page, wrt + "\n" + sectionText.substring(sectionText.indexOf("|}") + 2), "creating/updating", 0);

        db.stopDB();
    }
}
