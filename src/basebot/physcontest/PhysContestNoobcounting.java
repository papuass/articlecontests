/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.physcontest;

import java.util.HashSet;
import java.util.Set;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class PhysContestNoobcounting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.setUserAgent("BaseBot");
        w.login(args[0], args[1]);
        w.setMarkBot(true);
        w.setMarkMinor(true);

        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);

        String wrt = "List of [[wmua:WikiPhysContest-2016|]] participants\n\n\n";

        String[] cat = w.whatTranscludesHere("Шаблон:WikiPhysContest-2016", 1);

        Set<String> sigma = new HashSet<>();

        for (String talk : cat) {
            String tpt = w.getPageText(talk);
            //w.edit(talk, tpt, "purge");
            String regex = "(?s).*\\{\\{\\s*[Ww]ikiPhysContest-2016\\s*\\|[^}]*користувач\\s*=([^\\|}]+)[^}]*}}.*";
            String user = "";
            if (tpt.matches(regex)) {
                user = tpt.replaceAll(regex, "$1");
            } else {
                continue;
            }

            sigma.add(user);
        }
        String[] toArray = sigma.toArray(new String[sigma.size()]);

        Set<String> users = new HashSet<>();
        
        for (String string : toArray) {
            String user = w.userExists(string.substring(string.indexOf(":") + 1).trim())? string.substring(string.indexOf(":") + 1).trim():"";
            if (!user.equals("")) {
                users.add(user);
            }
            wrt += "\n# {{target | user=" + string.substring(string.indexOf(":") + 1) + "| site = uk.wikipedia.org }}";
        }
 
        for (String user : users) {
            System.out.println(user);
        }
        
        System.out.println(wrt);
        //m.edit("user:Base/MassMessage/WikiPhysContest-2016-participants", wrt, "creating/updating");
    }

}
