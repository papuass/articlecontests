/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basebot.physcontest;

import java.util.HashSet;
import java.util.Set;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class PhysContestMassMessage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.setUserAgent("BaseBot");
        w.login(args[0], args[1]);
        w.setMarkBot(true);
        w.setMarkMinor(true);
        
        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.setUserAgent("BaseBot");
        m.login(args[0], args[1]);
        m.setMarkBot(true);
        m.setMarkMinor(true);

        String wrt = "List of targets for one-time invitation of users marked as"
                + "[[:uk:Категорія:Користувачі — фізики|physicists]], [[:uk:Категорія:Інтерес:Фізика|interested in physics]], "
                + "[[:uk:Категорія:Інтерес:Астрономія|interested in astronomy]], "
                + "[[:uk:Категорія:Учасники проекту:Астрономія|wikiproject astronomy members]], "
                + "[[:uk:Шаблон:Учасник проекту:Фізика|wikiproject physics members]]) to partake in [[wmua:WikiPhysContest-2016|]]\n\n\n";

        String[] a = w.getCategoryMembers("Користувачі — фізики", 2);
        String[] b = w.getCategoryMembers("Інтерес:Фізика", 2);
        String[] c = w.getCategoryMembers("Інтерес:Астрономія", 2);
        String[] d = w.getCategoryMembers("Учасники проекту:Астрономія", 2);
        String[] e = w.whatTranscludesHere("Шаблон:Учасник проекту:Фізика", 2);
        String[][] abcde = {a, b, c, d, e};

        Set<String> sigma = new HashSet<>();

        for (String[] strings : abcde) {
            for (String cat : strings) {
                cat = cat.contains("/") ? cat.substring(0, cat.indexOf("/")) : cat;
                sigma.add(cat);
            }
        }
        String[] toArray = sigma.toArray(new String[sigma.size()]);

        for (String string : toArray) {
            wrt += "\n# {{target | user=" + string.substring(string.indexOf(":") + 1) + "| site = uk.wikipedia.org }}";
        }

        System.out.println(wrt);
        m.edit("user:Base/MassMessage/WikiPhysContest-2016-invitation", wrt, "creating/updating");
    }

}
