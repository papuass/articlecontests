SELECT `article_author`, points, week1author, week1 
FROM  (
    SELECT @row := @row + 1 row, `article_author`, sum(`article_mark`) as points
    FROM `2016-ukwiki-results`, (SELECT @row := 0) r
    GROUP BY `article_author`
    ORDER BY points desc
    ) subtabletotal
FULL OUTER JOIN (
    SELECT @row := @row + 1 row, `article_author`, sum(case when `article_week` = 1 then `article_mark` else 0 end) as week1
    FROM `2016-ukwiki-results`, (SELECT @row := 0) r
    GROUP BY `article_author`
    ORDER BY week1 desc
)  week1table ON subtabletotal.row = week1table.row

(
    SELECT `article_author`
    FROM (
        SELECT `article_author`, sum(`article_mark`) as points
        FROM `2016-ukwiki-results`
        GROUP BY `article_author`
        ORDER BY points desc
    ) as subsubtable
    WHERE 1
) as totalauthor,
(
    SELECT `article_author`, sum(`article_mark`) as points
    FROM `2016-ukwiki-results`
    GROUP BY `article_author`
    ORDER BY points desc
) as total
FROM `2016-ukwiki-results`
WHERE 1



,
(
SELECT `article_author`
FROM `2016-ukwiki-results`
GROUP BY `article_author`
ORDER BY week1 desc
) as week1author,
(
SELECT sum(case when `article_week` = 1 then `article_mark` else 0 end) as week1
FROM `2016-ukwiki-results`
GROUP BY `article_author`
ORDER BY week1 desc
) as week1


-----------------------------------------
------------------------------------------
SELECT (
    SELECT `article_author`
    FROM (
        SELECT `article_author`, sum(`article_mark`) as points
        FROM `2016-ukwiki-results`
        GROUP BY `article_author`
        ORDER BY points desc
    ) as subsubtable
    WHERE 1
) as totalauthor,
(
    SELECT `article_author`, sum(`article_mark`) as points
    FROM `2016-ukwiki-results`
    GROUP BY `article_author`
    ORDER BY points desc
) as total
FROM `2016-ukwiki-results`
WHERE 1



,
(
SELECT `article_author`
FROM `2016-ukwiki-results`
GROUP BY `article_author`
ORDER BY week1 desc
) as week1author,
(
SELECT sum(case when `article_week` = 1 then `article_mark` else 0 end) as week1
FROM `2016-ukwiki-results`
GROUP BY `article_author`
ORDER BY week1 desc
) as week1

sum(case when `article_week` = 1 then `article_mark` else 0 end) as week1,
sum(case when `article_week` = 1 then `article_mark_ceiled_quality` else 0 end) as ceiled_week1,
sum(case when `article_week` = 1 then `article_mark_quality_two` else 0 end) as q2_week1,
sum(case when `article_week` = 2 then `article_mark` else 0 end) as week2,
sum(case when `article_week` = 2 then `article_mark_ceiled_quality` else 0 end) as ceiled_week2,
sum(case when `article_week` = 2 then `article_mark_quality_two` else 0 end) as q2_week2,
sum(case when `article_week` = 3 then `article_mark` else 0 end) as week3,
sum(case when `article_week` = 3 then `article_mark_ceiled_quality` else 0 end) as ceiled_week3,
sum(case when `article_week` = 3 then `article_mark_quality_two` else 0 end) as q2_week3,
sum(case when `article_week` = 4 then `article_mark` else 0 end) as week4,
sum(case when `article_week` = 4 then `article_mark_ceiled_quality` else 0 end) as ceiled_week4,
sum(case when `article_week` = 4 then `article_mark_quality_two` else 0 end) as q2_week4,
sum(case when `article_week` = 5 then `article_mark` else 0 end) as week5,
sum(case when `article_week` = 5 then `article_mark_ceiled_quality` else 0 end) as ceiled_week5,
sum(case when `article_week` = 5 then `article_mark_quality_two` else 0 end) as q2_week5,
sum(case when `article_week` = 6 then `article_mark` else 0 end) as week6,
sum(case when `article_week` = 6 then `article_mark_ceiled_quality` else 0 end) as ceiled_week6,
sum(case when `article_week` = 6 then `article_mark_quality_two` else 0 end) as q2_week6,
sum(case when `article_week` = 7 then `article_mark` else 0 end) as week7,
sum(case when `article_week` = 7 then `article_mark_ceiled_quality` else 0 end) as ceiled_week7,
sum(case when `article_week` = 7 then `article_mark_quality_two` else 0 end) as q2_week7,
sum(case when `article_week` = 8 then `article_mark` else 0 end) as week8,
sum(case when `article_week` = 8 then `article_mark_ceiled_quality` else 0 end) as ceiled_week8,
sum(case when `article_week` = 8 then `article_mark_quality_two` else 0 end) as q2_week8,
sum(case when `article_week` = 9 then `article_mark` else 0 end) as week9,
sum(case when `article_week` = 9 then `article_mark_ceiled_quality` else 0 end) as ceiled_week9,
sum(case when `article_week` = 9 then `article_mark_quality_two` else 0 end) as q2_week9,
sum(case when `article_week` = 10 then `article_mark` else 0 end) as week10,
sum(case when `article_week` = 10 then `article_mark_ceiled_quality` else 0 end) as ceiled_week10,
sum(case when `article_week` = 10 then `article_mark_quality_two` else 0 end) as q2_week10
FROM `2016-ukwiki-results`
WHERE 1
GROUP BY `article_author`
ORDER BY points desc
