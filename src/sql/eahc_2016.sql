-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 25 2016 г., 01:00
-- Версия сервера: 5.5.27
-- Версия PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `eahc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `2016`
--

CREATE TABLE IF NOT EXISTS `2016` (
  `counter` int(11) NOT NULL AUTO_INCREMENT,
  `article_name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `article_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_ca` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `article_creationtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `article_language` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `article_country` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `article_wikidata` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`counter`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=602 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
