/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

/**
 *
 * @author Base < base-w@yandex.ru >
 */
public class Credentials {

    public String wikilogin;
    public String wikipassword;
    public String shelllogin;
    public String labsdblogin;
    public String labsdbpassword;
    public String sshkeypath;
    public String sshpassphrase;

    public Credentials(String wikilogin,
                       String wikipassword,
                       String shelllogin,
                       String labsdblogin,
                       String labsdbpassword,
                       String sshkeypath,
                       String sshpassphrase
    ) {
        this.wikilogin = wikilogin;
        this.wikipassword = wikipassword;
        this.shelllogin = shelllogin;
        this.labsdblogin = labsdblogin;
        this.labsdbpassword = labsdbpassword;
        this.sshkeypath = sshkeypath;
        this.sshpassphrase = sshpassphrase;
    }

    public Credentials(String args[]) {
        this(
                args[0],
                args[1],
                args[2],
                args[3],
                args[4],
                args[5],
                args[6]
        );
    }
}
