package objects;

import java.util.Calendar;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class EAHCElement {

    public String element;
    public String user;
    public String userid;
    public String userca;
    public Calendar timestamp;
    public String language;
    public String country;
    public String type; // label, description

    public EAHCElement(
            String element,
            String user,
            String userid,
            String userca,
            Calendar timestamp,
            String language,
            String country,
            String type) {
        this.element = element;
        this.user = user;
        this.userid = userid;
        this.userca = userca;
        this.timestamp = timestamp;
        this.language = language;
        this.country = country;
        this.type = type;
    }

}
