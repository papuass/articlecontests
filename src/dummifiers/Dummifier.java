/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dummifiers;

import basebot.ceespring.objects.CEESUser;
import java.util.Calendar;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Used to populate DB with some rubbish so that SQL queries could be tested
 * before the contest start
 * @todo Update to represent the final DB structure
 * @author Base <base-w at yandex.ru>
 */
public class Dummifier {

    public static void main(String[] args) throws Exception {

        CEESUser[] users = {
            //(int id, String username, String sex, boolean noob)
            new CEESUser("1", "1", "Base", "male", "no"),
            new CEESUser("2", "2", "Ата", "female", "no"),
            new CEESUser("3", "3", "Ochilov", "male", "yes"),
            new CEESUser("4", "4", "Лорд_Бъмбъри", "male", "no"),
            new CEESUser("5", "5", "Magalia", "female", "yes"),
            new CEESUser("6", "6", "Friend", "male", "no"),
            new CEESUser("7", "7", "NickK", "male", "no"),
            new CEESUser("8", "8", "Antanana", "female", "no"),
            new CEESUser("9", "9", "Spiritia", "female", "yes"),
            new CEESUser("10", "10", "Voll", "male", "no"),
            new CEESUser("11", "11", "Artificial123", "male", "yes")
        };

        String[] topics = {"Culture", "Geo", "Economics", "Society", "Sports",
            "Politics", "Transport", "History", "Science", "Education", "Women"};

        String[] countries = {"Latvia", "Macedonia", "Georgia", "Albania", "Bulgaria",
            "Armenia", "Estonia", "Ukraine", "Serbia", "Republic of Srpska",
            "Lithuania", "Esperantujo", "Belarus", "Greece", "Hungary",
            "Romania,", "Moldova", "Russia", "Austria", "Slovakia",
            "Czech Republic", "Russia, Bashkortostan", "Azerbaijan", "Poland"};

        String[] wikis = {"uk.wikipedia.org", "bg.wikipedia.org", "et.wikipedia.org", "lv.wikipedia.org", "be.wikipedia.org"};

        String[] words = Words.Words().split(",\\s");

        for (String article : words) {
            CEESUser user = users[random(0, users.length)];
            String username = user.name;
            String usergender = user.gender;
            String userid = user.identifier;
            String usernoob = user.isNoob;
            String wname = wikis[random(0, wikis.length)];

            int size = random(0, 80001);
            int sizewords = size / random(1, 51);

            String topic = topics[random(0, topics.length)];
            String country = countries[random(0, countries.length)];
            int boo = random(0, 2);
            int bool = random(0, 2);

            boolean isperson = boo == 1;
            boolean iswoman = isperson ? bool == 1 : false;
            boolean isfromlist = random(0, 2) == 1;
            int dayOfYear = random(1, 153);// generate a number between 1 and 365 (or 366 if you need to handle leap year);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, 2016);
            calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
            java.util.Date randomDoB = calendar.getTime();
            //            public static void Inserterie(String wiki, int article_id, String article_title,
            //            int author_id, int author_ca, String author_name, String author_gender,
            //            boolean author_isnoob, int article_bytes, int article_words,
            //            String article_country, String article_topic,
            //            boolean article_isperson, boolean article_iswoman, java.sql.Date article_creationdate,
            //            int article_wdentity, boolean article_fromlist
            //    )
//            ;

//            public static void Inserterie(String wiki, int article_id, String article_title,
//            int author_id, int author_ca, String author_name, String author_gender,
//            boolean author_isnoob, int article_bytes, int article_words,
//            String article_country, String article_topic,
//            boolean article_isperson, boolean article_iswoman, java.sql.Date article_creationdate,
//            int article_wdentity, boolean article_fromlist
//    )
//            DBpart.Inserterie(wname, i, article,
//                    userid, userid, username, usergender,
//                    usernoob, size, sizewords,
//                    country, topic,
//                    isperson, iswoman, new java.sql.Date(randomDoB.getTime()),
//                    i, isfromlist
//            );
        }
    }

    public static int random(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);

    }
}
