/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wikipedia;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class BaseBot extends WMFWiki {

    /**
     * Creates a new wiki that has the given domain name.
     *
     * @param domain a wiki domain name e.g. en.wikipedia.org
     */
    public BaseBot(String domain) {
        super(domain);
        this.setUserAgent("BaseBot");
        this.setMaxLag(1000);
        this.setMarkBot(true);
        this.setMarkMinor(true);
    }

    /**
     * @param title - title of the page in the site
     * @param site - the site the page is in
     * @return
     * @throws IOException
     */
    public boolean hasWDEntity(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "hasWDEntity");
        boolean r = false;
        if (!line.contains("missing=\"\"")) {
            r = true;
        }
        return r;
    }

    /**
     * Checks if a user has uploads between the specified times.
     *
     * @param user the user to get uploads for
     * @param start the date to start enumeration (use null to not specify one)
     * @param end the date to end enumeration (use null to not specify one)
     * @return a list of all live images the user has uploaded
     * @throws IOException if a network error occurs
     */
    public boolean hasUploads(Wiki.User user, Calendar start, Calendar end) throws IOException {
        StringBuilder url = new StringBuilder(query);
        url.append("list=allimages&ailimit=1&aisort=timestamp&aiprop=timestamp%7Ccomment&aiuser=");
        url.append(encode(user.getUsername(), false));
        if (start != null) {
            url.append("&aistart=");
            url.append(calendarToTimestamp(start));
        }
        if (end != null) {
            url.append("&aiend=");
            url.append(calendarToTimestamp(end));
        }
        List<Wiki.LogEntry> uploads = new ArrayList<>();

        String line = fetch(url.toString(), "getUploads");

        Boolean result = line.contains("<img ");

        log(Level.INFO, "getUploads", "Successfully retrieved uploads of\t" + user.getUsername() + "\t(\t" + result + "\t)");

        return result;
    }

    /**
     * @param id
     * @return
     * @throws IOException
     */
    public Map<String, String> getSiteLinks(String id) throws IOException {
        Map<String, String> data = new HashMap<>();
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&ids=";
        url += URLEncoder.encode(id, "UTF-8");
        String line = fetch(url, "getSiteLinks");
        if (line.contains("<sitelinks>")) {
            line = line.substring(line.indexOf("<sitelinks>"), line.indexOf("</sitelinks>"));
            String[] links = line.split("<sitelink ");

            for (int i = 1; i < links.length; i++) {
                String link = links[i];
                String site = parseAttribute(link, "site", 0);
                String title = parseAttribute(link, "title", 0);
                data.put(site, title);
            }
        }
        return data;
    }

    /**
     * @param title - title of the page in the site
     * @param site - the site the page is in
     * @return
     * @throws IOException
     */
    public int WDEntity(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "WDEntity");
        int r = 0;
        if (!line.contains("missing=\"\"")) {
            r = 0;
        }

        int a = line.indexOf(" id=\"Q") + 6;
        int b = line.indexOf('"', a);
        r = Integer.parseInt(line.substring(a, b));

        return r;
    }

    /**
     * Servs for resolving redirects
     *
     * @param qid
     * @return
     * @throws IOException
     */
    public String[] WDEntity(String... qid) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=&ids=";
        String ids = "";
        for (String qid1 : qid) {
            ids += "".equals(ids) ? qid1 : "|" + qid1;
        }
        url += ids;
        String line = fetch(url, "WDEntity");
        String r = "";
        if (line.contains("missing=\"\"")) {
            r = null;
        } else {

            boolean run = true;
            while (run) {
                int a = line.indexOf(" id=\"Q") + 6;
                int b = line.indexOf('"', a);
                r += "".equals(r) ? line.substring(a, b) : "|" + line.substring(a, b);
                line = line.substring(b);
                run = line.contains(" id=\"Q");
            }
        }

        return r.split("\\|");
    }

    public String[] WDEntityIsHuman(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = /*query*/ "https://www.wikidata.org/w/api.php?format=xml&maxlag=5&action=query&" + "action=wbgetentities&redirects=yes&props=sitelinks|claims&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "WDEntityIsHuman");
//        System.out.println(line);
        String id;
        String humanity = "no";
        String female = "no";
        if (line.contains("missing=\"\"")) {
            return null;
        }

        int a = line.indexOf(" id=\"Q") + 6;
        int b = line.indexOf('"', a);
        id = line.substring(a, b);

        if (line.contains("<property id=\"P31\">")) {
            int d = line.indexOf("<property id=\"P31\">");
            a = line.indexOf("<mainsnak", d);
            b = line.indexOf("</mainsnak>", a);
            String stack = line.substring(a, b);
            if (stack.contains("<value entity-type=\"item\" numeric-id=\"5\"")) {
                humanity = "yes";
            }
        }

        if (line.contains("<property id=\"P21\">")) {
            int d = line.indexOf("<property id=\"P21\">");
            a = line.indexOf("<mainsnak", d);
            b = line.indexOf("</mainsnak>", a);
            String snack = line.substring(a, b);
            if (snack.contains("<value entity-type=\"item\" numeric-id=\"6581072\"")) {
                female = "yes";
                System.out.println("FEMALE");

            } else {
                System.out.println("NOT A FEMALE IT SEEMS");
            }
        }

        String badge = "";
        String sitelinkTag = "<sitelink site=\"" + site + "\" title=\"" + title + "\">";
        a = line.indexOf(sitelinkTag) + sitelinkTag.length();
        b = line.indexOf("</sitelink>", a);
        String sitelink = line.substring(a, b);
        if (sitelink.contains("<badges/>")) {
        } else {
            String badgeRegex = "(?s).*<badge>([^<]*)</badge>.*".trim();
            badge = sitelink.replaceAll(badgeRegex, "$1");
        }

        String[] r = {id, humanity, female, badge};

        return r;
    }

    ;
    

    public int CAId(String user) throws IOException {
        //action=query&meta=globaluserinfo&guiuser=Example&format=xml
        String url = query + "meta=globaluserinfo&guiuser=" + URLEncoder.encode(user, "UTF-8");
        String line = fetch(url, "CAId");
        int r = 0;

        try {
            r = Integer.parseInt(parseAttribute(line, "id", 0));
        } catch (java.lang.NumberFormatException ex) {
            System.out.println("Error on " + url + "\n" + line);
        }

        return r;
    }

    public Map<String, String> batchPageTexts(String batchurl) throws Exception {
        //https://uk.wikipedia.org/w/api.php?action=query&format=xml&prop=revisions&list=&meta=&titles=Почтальон+Печкин|Ата&redirects=1&rvprop=content 
        String url = query + "format=xml&prop=revisions&rvprop=content"/*+"&titles=" + URLEncoder.encode(batchurl, "UTF-8")*/;
//        String line = fetch(url, "batchPageTexts");

        StringBuilder buffer = new StringBuilder(300000);
        buffer.append("titles=");
        buffer.append(URLEncoder.encode(batchurl, "UTF-8"));

        String line = post(url, buffer.toString(), "batchPageTexts");
        Map<String, String> returnie = new HashMap<>();
        int i = 0;
        boolean cont = false;
        cont = line.contains("<page ");
        while (cont) {
            //System.out.println(i++);
            String pageendtag = "</page>";
            int indexOfPageEnd = line.indexOf(pageendtag);
            if (indexOfPageEnd == -1) {
                break;
            }
            //System.out.println(indexOfPageEnd);

            String page = line.substring(0, indexOfPageEnd);
            line = line.substring(indexOfPageEnd + pageendtag.length());
            cont = line.contains("<page ");
            if (!page.contains("</rev>")) {
                continue;
            }
            String pagemetadata = page.replaceAll("(?s).*(<page[^>]*?>).*", "$1");
            //System.out.println(pagemetadata);
            String pagetitle = pagemetadata.replaceAll("(?s).*title=\"(.*?)\".*", "$1").trim();
            String pagetext = decode(page.replaceAll("(?s).*<rev[^>]*?>\n?(.*)\n?</rev>.*", "$1").trim());
            if ("".equals(pagetitle) || "".equals(pagetext)) {
                continue;
            }
            returnie.put(pagetitle, pagetext);
        }

        return returnie;

    }
}
